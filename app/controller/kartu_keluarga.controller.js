const library = require("../core/library");
const { Op } = require("sequelize");

exports.getAll = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.kartu_keluarga.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	if (!library.isEmpty(search.value)) {
		const datas = await library.model.kartu_keluarga.findAll({
			include: [
				{
					model: library.model.kecamatan,
					as: "kecamatan",
				},
				{
					model: library.model.wilayah_kerja,
					as: "wilayah_kerja",
				},
				{
					model: library.model.puskesmas,
					as: "puskesmas",
				},
				{
					model: library.model.anggota_keluarga,
					as: "anggota_keluarga",
				},
			],
			where: {
				[library.Op.or]: [
					{
						no_kk: {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$kecamatan.nama_kecamatan$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$puskesmas.nama_puskesmas$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$wilayah_kerja.nama_posyandu$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$anggota_keluarga.full_name$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
				],
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			const kepalaKeluarga = element.anggota_keluarga.filter(
				(item) => item.kepala_keluarga === true
			);

			const fullNames = kepalaKeluarga.map((item) => item.full_name);

			array.push([
				`${element.id}`,
				`${element.no_kk}`,
				`${fullNames}`,
				`${element.kecamatan.nama_kecamatan}`,
				`${element.puskesmas.nama_puskesmas}`,
				`${element.wilayah_kerja.nama_posyandu}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.kartu_keluarga.findAll({
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["createdAt", "ASC"]],
			include: [
				{
					model: library.model.kecamatan,
					as: "kecamatan",
				},
				{
					model: library.model.wilayah_kerja,
					as: "wilayah_kerja",
				},
				{
					model: library.model.puskesmas,
					as: "puskesmas",
				},
				{
					model: library.model.anggota_keluarga,
					as: "anggota_keluarga",
				},
			],
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			const kepalaKeluarga = element.anggota_keluarga.filter(
				(item) => item.kepala_keluarga === true
			);

			const fullNames = kepalaKeluarga.map((item) => item.full_name);

			array.push([
				`${element.id}`,
				`${element.no_kk}`,
				`${fullNames}`,
				`${element.kecamatan.nama_kecamatan}`,
				`${element.puskesmas.nama_puskesmas}`,
				`${element.wilayah_kerja.nama_posyandu}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.getAnggotaKeluarga = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.anggota_keluarga.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	id_kk = req.headers["referer"].split("=");

	console.log(id_kk);

	if (!library.isEmpty(search.value)) {
		const datas = await library.model.anggota_keluarga.findAll({
			where: {
				[library.Op.or]: [
					{
						full_name: {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						nik: {
							[library.Op.like]: `%${search.value}%`,
						},
					},
				],
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			var umur = await library.hitungUmur(element.tgl_lahir);

			array.push([
				`${element.id}`,
				`${element.nik}`,
				`${element.full_name}`,
				`${element.kepala_keluarga}`,
				`${element.anak_ke}`,
				`${element.is_anak}`,
				`${element.tempat_lahir}`,
				`${element.tgl_lahir}`,
				`${umur.tahun} tahun ${umur.bulan} Bulan ${umur.hari} Hari.`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.anggota_keluarga.findAll({
			where: {
				id_kk: id_kk[2],
			},
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["createdAt", "ASC"]],
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			var umur = await library.hitungUmur(element.tgl_lahir);

			array.push([
				`${element.id}`,
				`${element.nik}`,
				`${element.full_name}`,
				`${element.kepala_keluarga}`,
				`${element.anak_ke}`,
				`${element.is_anak}`,
				`${element.tempat_lahir}`,
				`${element.tgl_lahir}`,
				`${umur.tahun} tahun ${umur.bulan} Bulan ${umur.hari} Hari.`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.add = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (
				library.isEmpty(data.nomor_kk) &&
				library.isEmpty(data.kecamatan) &&
				library.isEmpty(data.wilayah_kerja) &&
				library.isEmpty(data.puskesmas)
			) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Data Belum Lengkap",
				});
			} else {
				const [kartu_keluarga, created] =
					await library.model.kartu_keluarga.findOrCreate({
						where: {
							no_kk: data.nomor_kk,
						},
						defaults: {
							id: await library.uuid(),
							no_kk: data.nomor_kk,
							id_kecamatan: data.kecamatan,
							id_puskesmas: data.puskesmas,
							id_wilayah_kerja: data.wilayah_kerja,
						},
					});

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `<b>Nomor KK ${kartu_keluarga.no_kk}</b> <br>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `<b>Nomor KK ${kartu_keluarga.no_kk}</b> <br>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.addAnggotaKeluarga = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			var defaults;

			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (
				library.isEmpty(data.nik) &&
				library.isEmpty(data.nama_lengkap) &&
				library.isEmpty(data.id_kk)
			) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Data Belum Lengkap",
				});
			} else {
				if (data.status_keluarga == "0" || data.status_keluarga == "1") {
					defaults = {
						id: await library.uuid(),
						nik: data.nik,
						id_kk: data.id_kk,
						full_name: data.nama_lengkap,
						status_keluarga: data.status_keluarga,
						kepala_keluarga: data.kepala_keluarga,
						anak_ke: "",
						is_anak: 0,
						tgl_lahir: data.tgl_lahir,
						tempat_lahir: data.tempat_lahir,
						no_hp: data.no_hp,
					};
				} else if (data.status_keluarga == "2") {
					defaults = {
						id: await library.uuid(),
						nik: data.nik,
						id_kk: data.id_kk,
						full_name: data.nama_lengkap,
						status_keluarga: data.status_keluarga,
						kepala_keluarga: data.kepala_keluarga,
						anak_ke: data.anak_ke,
						is_anak: 1,
						tgl_lahir: data.tgl_lahir,
						tempat_lahir: data.tempat_lahir,
						no_hp: data.no_hp,
					};
				}

				const [anggota_keluarga, created] =
					await library.model.anggota_keluarga.findOrCreate({
						where: {
							nik: data.nik,
						},
						defaults: defaults,
					});

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `Anggota Keluarga<br>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `Anggota Keluarga<br>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
