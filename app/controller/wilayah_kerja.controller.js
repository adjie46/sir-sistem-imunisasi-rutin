const library = require("../core/library");
const { Op } = require("sequelize");

exports.getAll = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.wilayah_kerja.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	id_puskesmas = req.headers["referer"].split("=");

	if (!library.isEmpty(search.value)) {
		const datas = await library.model.wilayah_kerja.findAll({
			where: {
				[library.Op.and]: [
					{
						nama_posyandu: {
							[library.Op.like]: `%${search.value.toUpperCase()}%`,
						},
					},
					{
						id_puskesmas: id_puskesmas[2],
					},
				],
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.nama_posyandu}`,
				`${element.nama_desa}`,
				`${element.status}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.wilayah_kerja.findAll({
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["nama_posyandu", "ASC"]],
			where: {
				id_puskesmas: id_puskesmas[2],
			},
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.nama_posyandu}`,
				`${element.nama_desa}`,
				`${element.status}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.add = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (
				library.isEmpty(data.nama_posyandu) &&
				library.isEmpty(data.nama_desa)
			) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Nama Posyandu & Desa Tidak Boleh Kosong",
				});
			} else {
				const [wilayah_kerja, created] =
					await library.model.wilayah_kerja.findOrCreate({
						where: {
							nama_posyandu: data.nama_posyandu,
						},
						defaults: {
							id: await library.uuid(),
							id_puskesmas: data.id_puskesmas,
							nama_posyandu: data.nama_posyandu.trim().toUpperCase(),
							nama_desa: data.nama_desa.trim().toUpperCase(),
							status: 1,
						},
					});

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `<b>${wilayah_kerja.nama_posyandu}</b> <br>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `<b>${wilayah_kerja.nama_posyandu}</b> <br>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.deleteById = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));
			id = req.params.id;
			if (library.isEmpty(id)) {
				return res.status(library.responseStatus.OK).json({
					success: false,
					code: library.responseStatus.OK,
					message: `Data Wilayah Kerja Tidak Ditemukan`,
					status: 3,
				});
			} else {
				const wilayah_kerja = await library.model.wilayah_kerja.destroy({
					where: {
						id: id,
					},
				});

				if (wilayah_kerja) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `Wilayah Kerja Berhasil Dihapus`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `Wilayah Kerja <br>Gagal dihapus`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.getById = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));
			id = req.params.id;
			if (library.isEmpty(id)) {
				return res.status(library.responseStatus.OK).json({
					success: false,
					code: library.responseStatus.OK,
					message: `Format Salah!`,
					status: 3,
				});
			} else {
				const wilayah_kerja = await library.model.wilayah_kerja.findAll({
					where: {
						id_puskesmas: id,
					},
				});

				if (wilayah_kerja) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `Data Wilayah Kerja Ditemukan`,
						data: wilayah_kerja,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `Data wilayah_kerja Tidak di Temukan`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
