const library = require("../core/library");
const { Op } = require("sequelize");

exports.getAll = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.kartu_keluarga.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	if (!library.isEmpty(search.value)) {
		const datas = await library.model.jadwal_imunisasi.findAll({
			include: [
				{
					model: library.model.kecamatan,
					as: "kecamatan",
				},
				{
					model: library.model.wilayah_kerja,
					as: "wilayah_kerja",
				},
				{
					model: library.model.puskesmas,
					as: "puskesmas",
				},
			],
			where: {
				[library.Op.or]: [
					{
						no_kk: {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$kecamatan.nama_kecamatan$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$puskesmas.nama_puskesmas$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$wilayah_kerja.nama_posyandu$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
					{
						"$anggota_keluarga.full_name$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
				],
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.tgl_imunisasi_format}`,
				`${element.kecamatan.nama_kecamatan}`,
				`${element.puskesmas.nama_puskesmas}`,
				`${element.wilayah_kerja.nama_posyandu}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.jadwal_imunisasi.findAll({
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["createdAt", "ASC"]],
			include: [
				{
					model: library.model.kecamatan,
					as: "kecamatan",
				},
				{
					model: library.model.wilayah_kerja,
					as: "wilayah_kerja",
				},
				{
					model: library.model.puskesmas,
					as: "puskesmas",
				},
			],
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.tgl_imunisasi_format}`,
				`${element.kecamatan.nama_kecamatan}`,
				`${element.puskesmas.nama_puskesmas}`,
				`${element.wilayah_kerja.nama_posyandu}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.add = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (
				library.isEmpty(data.tgl_imunisasi) &&
				library.isEmpty(data.kecamatan) &&
				library.isEmpty(data.wilayah_kerja) &&
				library.isEmpty(data.puskesmas)
			) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Data Belum Lengkap",
				});
			} else {
				const [jadwal_imunisasi, created] =
					await library.model.jadwal_imunisasi.findOrCreate({
						where: {
							tgl_imunisasi: data.tgl_imunisasi,
						},
						defaults: {
							id: await library.uuid(),
							tgl_imunisasi: data.tgl_imunisasi,
							id_kecamatan: data.kecamatan,
							id_puskesmas: data.puskesmas,
							id_wilayah_kerja: data.wilayah_kerja,
						},
					});

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `<b>Jadwal ${jadwal_imunisasi.tgl_imunisasi}</b> <br>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `<b>Jadwal ${jadwal_imunisasi.tgl_imunisasi}</b> <br>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

function mergeDataByAnakAndJenisImun(data) {
	const mergedData = {};

	data.forEach((item) => {
		const anakKey = item.anak[0].nama_anak;
		const jenisImunKey = item.anak[0].jenis_imun[0].name;

		if (!mergedData[anakKey]) {
			mergedData[anakKey] = { ...item };
			mergedData[anakKey].anak[0].jenis_imun = [{ name: jenisImunKey }];
		} else {
			// Check if jenis_imun already exists for the anak
			const jenisImunExists = mergedData[anakKey].anak[0].jenis_imun.some(
				(jenis) => jenis.name === jenisImunKey
			);

			// If jenis_imun doesn't exist, add it
			if (!jenisImunExists) {
				mergedData[anakKey].anak[0].jenis_imun.push({ name: jenisImunKey });
			}
		}
	});

	// Convert the mergedData object back to an array
	const mergedArray = Object.values(mergedData);
	return mergedArray;
}

// exports.sendNotif = async (req, res) => {
// 	return new Promise(async (resolve, reject) => {
// 		try {
// 			await library.formData(req, res);
// 			data = JSON.parse(JSON.stringify(req.body));

// 			var nama_anak;
// 			var nama_ayah;
// 			var nama_ibu;
// 			var nomor_hp;
// 			var tgl_imunisasi;
// 			var jenis_imunisasi;

// 			var data_pesan = [];
// 			var jenis_imun = [];
// 			var anak = [];
// 			var newData;

// 			const todayWithoutTime = new Date();
// 			todayWithoutTime.setHours(0, 0, 0, 0);

// 			getJadwal = await library.model.jadwal_imunisasi.findAll({
// 				where: {
// 					tgl_imunisasi: {
// 						[library.Op.gte]: todayWithoutTime, // Tanggal hari ini (tanpa waktu)
// 					},
// 				},
// 			});

// 			getDataImun = await library.model.jenis_imunisasi.findAll();

// 			await library.asyncForEach(getJadwal, async (element, index) => {
// 				await library.waitFor(1);

// 				const tanggalImunisasi = new Date(element.tgl_imunisasi); // Ganti dengan tanggal imunisasi yang sesuai
// 				tanggalImunisasi.setHours(0, 0, 0, 0);

// 				// Tanggal H-1 dari tanggal imunisasi
// 				const yesterdayWithoutTime = new Date(tanggalImunisasi);
// 				yesterdayWithoutTime.setDate(tanggalImunisasi.getDate() - 1);

// 				// Tanggal H-2 dari tanggal imunisasi
// 				const twoDaysAgoWithoutTime = new Date(tanggalImunisasi);
// 				twoDaysAgoWithoutTime.setDate(tanggalImunisasi.getDate() - 2);

// 				// Cocokkan dengan tanggal hari ini
// 				if (
// 					todayWithoutTime.getTime() === yesterdayWithoutTime.getTime() ||
// 					todayWithoutTime.getTime() === twoDaysAgoWithoutTime.getTime()
// 				) {
// 					//JIKA H-1
// 					setting_time = library.config.time_send.split(",");
// 					tgl_imunisasi = element.tgl_imunisasi_format;

// 					getKK = await library.model.kartu_keluarga.findAll({
// 						include: [
// 							{
// 								model: library.model.kecamatan,
// 								as: "kecamatan",
// 							},
// 							{
// 								model: library.model.wilayah_kerja,
// 								as: "wilayah_kerja",
// 							},
// 							{
// 								model: library.model.puskesmas,
// 								as: "puskesmas",
// 							},
// 							{
// 								model: library.model.anggota_keluarga,
// 								as: "anggota_keluarga",
// 							},
// 						],
// 						where: {
// 							[library.Op.and]: [
// 								{
// 									id_kecamatan: element.id_kecamatan,
// 								},
// 								{
// 									id_puskesmas: element.id_puskesmas,
// 								},
// 								{
// 									id_wilayah_kerja: element.id_wilayah_kerja,
// 								},
// 							],
// 						},
// 					});

// 					await library.asyncForEach(getKK, async (element, index) => {
// 						await library.waitFor(1);

// 						await library.asyncForEach(
// 							element.anggota_keluarga,
// 							async (element, index) => {
// 								await library.waitFor(1);

// 								if (element.status_keluarga == "1") {
// 									nama_ibu = element.full_name;
// 									nomor_hp = element.no_hp;
// 								}

// 								if (element.status_keluarga == "0") {
// 									nama_ayah = element.full_name;
// 								}

// 								if (element.status_keluarga == "2") {
// 									nama_anak = element.full_name;

// 									//JIKA PUNYA ANAK

// 									var umur = await library.hitungUmur(element.tgl_lahir);

// 									await library.asyncForEach(
// 										getDataImun,
// 										async (element, index) => {
// 											await library.waitFor(1);

// 											jenis_imun.push({
// 												name: element.jenis_imunisasi,
// 											});

// 											anak.push({
// 												nama_anak: nama_anak,
// 												umur: umur,
// 												jenis_imun,
// 											});

// 											if (umur.bulan == element.umur) {
// 												newData = {
// 													nama_ayah: nama_ayah,
// 													nama_ibu: nama_ibu,
// 													nomor_hp: nomor_hp,
// 													anak: anak,
// 												};
// 											}

// 											data_pesan.push(newData);
// 											jenis_imun = [];
// 											anak = [];
// 										}
// 									);
// 								} else {
// 									//console.log("TIDAK PUNYA ANAK");
// 								}
// 							}
// 						);
// 					});
// 				}
// 			});

// 			az = library.removeDuplicates(data_pesan);
// 			const jsonDataWithoutNull = az.filter((item) => item !== undefined);

// 			const mergedDataArray = mergeDataByAnakAndJenisImun(jsonDataWithoutNull);

// 			await library.asyncForEach(mergedDataArray, async (element, index) => {
// 				await library.waitFor(1);

// 				message_notif = `
// Hallo bunda *${element.nama_ibu}*

// Kami ingin memberikan pengingat yang penting tentang kesehatan dan jadwal imunisasi *${element.anak[0].nama_anak}* yang sudah berumur ${element.anak[0].umur.tahun} Tahun ${element.anak[0].umur.bulan} Bulan

// Berikut ini informasi yang perlu anda ketahui

// Tanggal imunisasi: ${tgl_imunisasi}
// Jenis imunisasi :
// `;
// 				var x;
// 				element.anak.forEach((element) => {
// 					x = element.jenis_imun.map((item) => `✅ ${item.name}`).join("\n");
// 				});

// 				message_imun = x;

// 				message_lanjut = `
// Imunisasi ini penting untuk melindungi anak dari penyakit berbahaya.pastikan anak dalam keadaan sehat saat datang ke palayanan imunisasi

// Berikut informasi penting yang harus ayah bunda ketahui
// 1. Jenis Antigen Sesuai Usia
// - Usia  <24 jam Hepatitis b
// - Usia 1 bulan BCG-polio tetes 1
// - Usia 2 bulan DPT-Hb-Hib +polio tetes 2+ PCV 1 + Rotavirus 1
// - Usia 3 bulan DPT-Hb-Hib +polio tetes 3+ PCV 2 + Rotavirus 2
// - Usia 4 bln DPT-Hb-Hib +polio tetes 4+IPV 1 + Rotavirus 3
// - Usia 9 bulan campak + IPV 2
// - Usia 12 bulan PCV 3

// 2. Reaksi Setelah Imunisasi
// Beberapa reaksi ringan seperti sedikit demam atau kemerahan ditempat suntikan bisa terjadi setelah imunisasi. Ini adalah reaksi normal dan akan hilang dengan sendirinya.

// 3. Jika bekas suntikan hanya bengkak kompres dengan air hangat,jika bengkak  kemerahan disertai panas kompres dengan air dingin,jika diperlukan minumlah obat demam sesuai anjuran dokter.
// Jika terus berlanjut lebih dari 3 hari segera laporkan ke petugas kesehatan setempat untuk mendapatkan penanganan lebih lanjut.

// Ingat lah bahwa kesehatan *${element.anak[0].nama_anak}* adalah prioritas  utama kami ,imunisasi adalah  langkah proaktif dalam melindungi *${element.anak[0].nama_anak}* dan juga kontribusi positif bagi kesehatan masyarakat.

// Terimakasih atas perhatian dan kerja sm ayah bunda,jika anda membutuhkan informasi lebih lanjut jangan ragu untuk menghubungi kami.

// Salam sehat
// 				`;

// 				if (element.nomor_hp != "-") {
// 					/* const mergedString = message_notif + message_imun + message_lanjut;
// 					library.sendWa(mergedString, element.nomor_hp); */
// 				}
// 			});

// 			return res.status(library.responseStatus.OK).send({
// 				success: true,
// 				code: library.responseStatus.OK,
// 				message: "OKE",
// 				data: mergedDataArray,
// 			});
// 		} catch (error) {
// 			return res.status(library.responseStatus.serverError).send({
// 				success: false,
// 				code: library.responseStatus.serverError,
// 				message: error.message,
// 			});
// 		}
// 	});
// };

exports.sendNotif = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			var next = false;
			var currentJadwal;
			var nama_ayah, nama_ibu, nama_anak, nomor_hp;
			var data_pesan = [];
			var sudah_imunisasi;
			var kecamatan;
			var puskesmas;
			var posyandu;

			const todayWithoutTime = new Date("2023-10-09");
			todayWithoutTime.setHours(0, 0, 0, 0);

			getJadwal = await library.model.jadwal_imunisasi.findAll({
				where: {
					tgl_imunisasi: {
						[library.Op.gte]: todayWithoutTime, // Tanggal hari ini (tanpa waktu)
					},
				},
			});

			await library.asyncForEach(getJadwal, async (element, index) => {
				await library.waitFor(1);

				const tanggalImunisasi = new Date(element.tgl_imunisasi);
				tanggalImunisasi.setHours(0, 0, 0, 0);

				//H-1
				const yesterdayWithoutTime = new Date(tanggalImunisasi);
				yesterdayWithoutTime.setDate(tanggalImunisasi.getDate() - 1);

				//H-2
				const twoDaysAgoWithoutTime = new Date(tanggalImunisasi);
				twoDaysAgoWithoutTime.setDate(tanggalImunisasi.getDate() - 2);

				if (
					todayWithoutTime.getTime() === yesterdayWithoutTime.getTime() ||
					todayWithoutTime.getTime() === twoDaysAgoWithoutTime.getTime()
				) {
					next = true;
					currentJadwal = element;
				} else if (todayWithoutTime.getTime() === tanggalImunisasi.getTime()) {
					next = true;
					currentJadwal = element;
				} else {
					next = false;
				}
			});

			if (next) {
				getKK = await library.model.kartu_keluarga.findAll({
					include: [
						{
							model: library.model.kecamatan,
							as: "kecamatan",
						},
						{
							model: library.model.wilayah_kerja,
							as: "wilayah_kerja",
						},
						{
							model: library.model.puskesmas,
							as: "puskesmas",
						},
						{
							model: library.model.anggota_keluarga,
							as: "anggota_keluarga",
						},
					],
					where: {
						[library.Op.and]: [
							{
								id_kecamatan: currentJadwal.id_kecamatan,
							},
							{
								id_puskesmas: currentJadwal.id_puskesmas,
							},
							{
								id_wilayah_kerja: currentJadwal.id_wilayah_kerja,
							},
						],
					},
					order: [[library.model.anggota_keluarga, "status_keluarga", "asc"]],
				});

				await library.asyncForEach(getKK, async (element, index) => {
					await library.waitFor(1);

					var newData;
					var umur;
					var myId;

					kecamatan = element.kecamatan.nama_kecamatan;
					puskesmas = element.puskesmas.nama_puskesmas;
					posyandu = element.wilayah_kerja.nama_posyandu;

					await library.asyncForEach(
						element.anggota_keluarga,
						async (element, index) => {
							await library.waitFor(1);

							if (element.status_keluarga == "1") {
								nama_ibu = element.full_name;
								nomor_hp = element.no_hp;
							}
							if (element.status_keluarga == "0") {
								nama_ayah = element.full_name;
							}
							if (element.status_keluarga == "2") {
								myId = element.id;
								nama_anak = element.full_name;
								umur = await library.hitungUmur(element.tgl_lahir);
							}

							newData = {
								id: myId,
								nama_ayah: nama_ayah,
								nama_ibu: nama_ibu,
								nomor_hp: nomor_hp,
								nama_anak: nama_anak,
								umur_anak: umur,
								kecamatan: kecamatan,
								puskesmas: puskesmas,
								posyandu: posyandu,
							};
						}
					);

					data_pesan.push(newData);
				});

				gabung = JSON.parse(JSON.stringify(data_pesan));

				await library.asyncForEach(data_pesan, async (element, index) => {
					await library.waitFor(1);

					hasImun = await library.model.anak_has_imun.findAll({
						attributes: ["tgl_imun"],
						include: [
							{
								attributes: ["jenis_imunisasi", "umur", "satuan_umur"],
								model: library.model.jenis_imunisasi,
								as: "jenis_imunisasi",
							},
						],
						where: {
							id_anak: element.id,
						},
						order: [[library.model.jenis_imunisasi, "createdAt", "asc"]],
					});

					gabung[index].imunisasi_yang_sudah = hasImun;
				});

				await library.asyncForEach(gabung, async (element, index) => {
					await library.waitFor(1);

					var data_tidak_ada = [];

					getDataImun = await library.model.jenis_imunisasi.findAll({
						attributes: ["jenis_imunisasi", "umur", "satuan_umur"],
						where: {
							umur: {
								[library.Op.lte]: element.umur_anak.bulan,
							},
						},
					});

					for (const imun of getDataImun) {
						const jenis_imunisasi_imun = imun.jenis_imunisasi;
						sudah_imunisasi = element.imunisasi_yang_sudah.some((item) => {
							return (
								item.jenis_imunisasi.jenis_imunisasi === jenis_imunisasi_imun
							);
						});
						if (!sudah_imunisasi) {
							data_tidak_ada.push(imun);
						}
					}

					const imunisasi_yang_belum_tanpa_duplikat = Array.from(
						new Set(data_tidak_ada.map(JSON.stringify))
					).map(JSON.parse);

					gabung[index].imunisasi_yang_belum =
						imunisasi_yang_belum_tanpa_duplikat;
				});

				await library.asyncForEach(gabung, async (element, index) => {
					await library.waitFor(1);

					const yang_belum = element.imunisasi_yang_belum
						.map((data) => `*❌ Vaksin ${data.jenis_imunisasi}*`)
						.join("\n");

					const yang_sudah = element.imunisasi_yang_sudah
						.map((data) => `*✅ ${data.jenis_imunisasi.jenis_imunisasi}*`)
						.join("\n");

					messages = `
Halo  Bunda ${element.nama_ibu} Kami harap Anda baik-baik saja.
Kami ingin mengingatkan Anda tentang imunisasi penting untuk si kecil (${element.nama_anak}) yang sekarang sudah berumur ${element.umur_anak.tahun} Tahun ${element.umur_anak.bulan} Bulan ${element.umur_anak.hari} Hari yang mungkin sudah terlewat.
Kesehatan anak adalah prioritas utama, dan imunisasi adalah langkah penting dalam menjaga mereka tetap sehat

Berikut adalah imunisasi yang Sudah didapat oleh ${element.nama_anak} : 
${yang_sudah}

Berikut adalah imunisasi yang Belum didapat oleh ${element.nama_anak} : 
${yang_belum}

Kami memahami bahwa jadwal yang padat dan banyak tanggung jawab dapat membuat hal-hal terlewat.
Namun, imunisasi adalah cara terbaik untuk melindungi anak dari penyakit yang dapat dicegah dengan Imunisasi.

Mohon untuk mengunjungi  ${element.puskesmas} / Bidan Desa saat Posyandu berlansung untuk pendapatkan layanan imunisasi yang sesuai.

Jika Anda memiliki pertanyaan atau perlu informasi lebih lanjut tentang imunisasi anak, jangan ragu untuk menghubungi kami.
Terima kasih atas perhatiannya terhadap kesehatan si kecil. Semoga mereka selalu sehat dan bahagia.

Salam hangat, 
${element.posyandu}

`;

					gabung[index].messages = messages;

					if (element.nomor_hp != "-") {
						//console.log(index + 1);
						//library.sendWa(messages, `62${element.nomor_hp}`);
						/* setTimeout(function () {
							// Kode yang akan dijalankan setelah penundaan 30 detik
							console.log("30 detik telah berlalu!");
							library.sendWa(messages, `62${element.nomor_hp}`);
							library.sendWa("OK", `6281269494591}`);
						}, 30000); // 30000 milidetik sama dengan 30 detik */
					}
				});

				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "OK",
					data: gabung,
				});
			} else {
				console.log("TIDAK LANJUT");
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.sendWa = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			messages = `
			Halo  Bunda Nuraini Kami harap Anda baik-baik saja.
Kami ingin mengingatkan Anda tentang imunisasi penting untuk si kecil (Muhammad Zifanussyauqie Dhaifullah) yang sekarang sudah berumur 0 Tahun 4 Bulan 28 Hari yang mungkin sudah terlewat.
Kesehatan anak adalah prioritas utama, dan imunisasi adalah langkah penting dalam menjaga mereka tetap sehat

Berikut adalah imunisasi yang Sudah didapat oleh Muhammad Zifanussyauqie Dhaifullah : 
*✅ HEPATITIS B*
*✅ BCG*
*✅ OPV 1*
*✅ DPT-HB-HIB1*
*✅ OPV2*

Berikut adalah imunisasi yang Belum didapat oleh Muhammad Zifanussyauqie Dhaifullah : 
*❌ Vaksin OPV4*
*❌ Vaksin DPT-HB-HIB2*
*❌ Vaksin RV1*
*❌ Vaksin RV3*
*❌ Vaksin PCV2*
*❌ Vaksin PCV1*
*❌ Vaksin IPV1*
*❌ Vaksin DPT-HB-HIB3*
*❌ Vaksin RV2*
*❌ Vaksin OPV3*

Kami memahami bahwa jadwal yang padat dan banyak tanggung jawab dapat membuat hal-hal terlewat.
Namun, imunisasi adalah cara terbaik untuk melindungi anak dari penyakit yang dapat dicegah dengan Imunisasi.

Mohon untuk mengunjungi  PUSKESMAS RANTAU / Bidan Desa saat Posyandu berlansung untuk pendapatkan layanan imunisasi yang sesuai.

Jika Anda memiliki pertanyaan atau perlu informasi lebih lanjut tentang imunisasi anak, jangan ragu untuk menghubungi kami.
Terima kasih atas perhatiannya terhadap kesehatan si kecil. Semoga mereka selalu sehat dan bahagia.

Salam hangat, 
POSYANDU DADAP
			`;

			var op = await library.sendWa(messages, "6282163477370");
			console.log(op);
		} catch (error) {
			console.log(error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
