const library = require("../core/library");
const { Op } = require("sequelize");

exports.getAll = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.jenis_imunisasi.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	if (!library.isEmpty(search.value)) {
		const datas = await library.model.jenis_imunisasi.findAll({
			where: {
				jenis_imunisasi: {
					[library.Op.like]: `%${search.value}%`,
				},
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.jenis_imunisasi}`,
				`${element.umur}`,
				`${element.satuan_umur}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.jenis_imunisasi.findAll({
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["umur", "ASC"]],
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.jenis_imunisasi}`,
				`${element.umur}`,
				`${element.satuan_umur}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.add = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (library.isEmpty(data.jenis_imun)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Data Tidak Lengkap ",
				});
			} else {
				const [jenis_imun, created] =
					await library.model.jenis_imunisasi.findOrCreate({
						where: {
							jenis_imunisasi: data.jenis_imun,
						},
						defaults: {
							id: await library.uuid(),
							jenis_imunisasi: data.jenis_imun.trim().toUpperCase(),
							umur: data.umur_imun.trim().toUpperCase(),
							satuan_umur: data.satuan_imun.trim().toUpperCase(),
						},
					});

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `<b>${jenis_imun.jenis_imunisasi}</b> <br>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `<b>${jenis_imun.jenis_imunisasi}</b> <br>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.getDataImunisasi = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.anak_has_imun.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	id_anak = req.headers["referer"].split("=");

	if (!library.isEmpty(search.value)) {
		let datas = await library.model.anak_has_imun.findAll({
			include: [
				{
					model: library.model.anggota_keluarga,
					as: "anggota_keluarga",
				},
				{
					model: library.model.jenis_imunisasi,
					as: "jenis_imunisasi",
				},
			],
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["tgl_imun", "ASC"]],
			where: {
				[library.Op.and]: [
					{
						id_anak: id_anak[2],
					},
					{
						"$anggota_keluarga.full_name$": {
							[library.Op.like]: `%${search.value}%`,
						},
					},
				],
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.anggota_keluarga[0].full_name}`,
				`${element.jenis_imunisasi.jenis_imunisasi}`,
				`${element.tgl_imunisasi_format}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.anak_has_imun.findAll({
			include: [
				{
					model: library.model.anggota_keluarga,
					as: "anggota_keluarga",
				},
				{
					model: library.model.jenis_imunisasi,
					as: "jenis_imunisasi",
				},
			],
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["tgl_imun", "ASC"]],
			where: {
				id_anak: id_anak[2],
			},
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.anggota_keluarga[0].full_name}`,
				`${element.jenis_imunisasi.jenis_imunisasi}`,
				`${element.tgl_imunisasi_format}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.addImunisasi = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (library.isEmpty(data.nama_anak)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Data Tidak Lengkap ",
				});
			} else {
				newDataImun = {
					id: await library.uuid(),
					id_anak: data.nama_anak,
					id_imun: data.jenis_imunisasi,
					tgl_imun: data.tgl_imunisasi,
				};

				created = await library.model.anak_has_imun.create(newDataImun);

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `<b>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `<b>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
