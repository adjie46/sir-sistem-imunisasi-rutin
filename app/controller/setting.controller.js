const library = require("../core/library");

exports.setting = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		const { id, action } = req.params;

		if (action == "android") {
			let { draw, start, length, search } = req.query;
			let pageNumber = 1;

			let countData = await library.model.app_setting.count();
			let array = [];

			if (draw == 1) {
				pageNumber = 1;
			} else {
				pageNumber = 1;
				pageNumber += start++;
			}

			if (!library.isEmpty(search.value)) {
				const datas = await library.model.app_setting.findAll({
					where: {
						app_name: {
							[Op.like]: `%${search.value}%`,
						},
					},
				});

				countData = datas.length;

				await library.asyncForEach(datas, async (element, index) => {
					await library.waitFor(1);

					array.push([
						`${element.id}`,
						`${index + 1}`,
						`${element.app_name}`,
						`${element.app_package}`,
						`${element.app_type}`,
						`${library.truncate(element.app_token, "8")}`,
					]);
				});

				return res.status(200).json({
					draw: draw,
					recordsTotal: countData,
					recordsFiltered: countData,
					data: array,
				});
			} else {
				start = req.query.start;

				let datas = await library.model.app_setting.findAll({
					offset: parseInt(start),
					limit: parseInt(length),
					order: [["app_name", "ASC"]],
				});

				await library.asyncForEach(datas, async (element, index) => {
					await library.waitFor(1);

					array.push([
						`${element.id}`,
						`${index + 1}`,
						`${element.app_name}`,
						`${element.app_package}`,
						`${element.app_type}`,
						`${library.truncate(element.app_token, "8")}`,
					]);
				});

				return res.status(200).json({
					draw: draw,
					recordsTotal: countData,
					recordsFiltered: countData,
					data: array,
				});
			}
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.delete = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		const { id, action } = req.params;

		library.model.app_setting.destroy({
			where: {
				id: id,
			},
		});

		return res.status(library.responseStatus.OK).send({
			success: true,
			code: library.responseStatus.OK,
			message: "Data Berhasil dihapus",
		});
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.copy = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		const { id, action } = req.params;

		apiKey = await library.model.app_setting.findOne({
			attributes: ["app_token"],
			where: {
				id: id,
			},
		});

		if (!library.isEmpty(apiKey)) {
			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "API KEY Berhasil di copy",
				data: apiKey,
			});
		} else {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message: "Data tidak ditemukan",
				data: {},
			});
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.add = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		const { id, action } = req.params;

		if (action == "android") {
			newAPI = {
				id: await library.uuid(),
				app_name: datas.app_name,
				app_package: datas.app_package,
				app_type: datas.app_type,
				app_token: await library.apiKeyGenerator("-", 6, 2),
			};

			const [appSetting, created] =
				await library.model.app_setting.findOrCreate({
					where: {
						app_package: datas.app_package,
					},
					defaults: newAPI,
				});

			if (created) {
				return res.status(library.responseStatus.OK).json({
					success: true,
					code: library.responseStatus.OK,
					message: `API KEY Berhasil ditambah`,
				});
			} else {
				return res.status(library.responseStatus.OK).json({
					success: false,
					code: library.responseStatus.OK,
					message: `API KEY Gagal ditambah`,
				});
			}
		} else {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message: "Format Salah",
			});
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};
