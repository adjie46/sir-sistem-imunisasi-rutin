const library = require("../core/library");

exports.dashboard_pages = async (req, res) => {
	let pages = req.query;
	let dataDashboard = {};
	let token = req.session.tokenLogin;
	let payload = req.decrypted.payload;

	myProfile = await library.model.auth.findOne({
		attributes: ["id", "user_type", "status"],
		include: [
			{
				model: library.model.profile,
				as: "profile",
			},
		],
		where: {
			id: await library.decrypt(payload.user_login_auth),
		},
	});

	if (myProfile.user_type == "-1") {
		user_type_category = "Operator Utama";
	} else {
		user_type_category = "";
	}

	if (
		library.fs.existsSync(
			`public/upload/photoProfile/${
				myProfile.profile.photo_profile.split("/")[4]
			}`
		)
	) {
		photoProfile = myProfile.profile.photo_profile;
	} else {
		photoProfile = "/dist/images/profile/user-1.jpg";
	}

	if (library.isEmpty(myProfile.profile.opd)) {
		opdName = "-";
	} else {
		opdName = myProfile.profile.opd.opd_name;
	}
	dataDashboard.users = {
		user_name: myProfile.profile.full_name,
		user_type: myProfile.user_type,
		user_type_category: user_type_category,
		user_email: myProfile.profile.email,
		user_phone: myProfile.profile.phone,
		user_photo: photoProfile,
		opd_id: myProfile.profile.opd_id,
		opd_name: opdName,
		id: await library.encrypt(myProfile.id),
	};

	if (library.isEmpty(pages)) {
		dataDashboard.current_pages = "dashboard";
	} else {
		dataDashboard.current_pages = pages.pages;
	}

	var fileContents;
	var showPages, showCss, showJs;

	try {
		fileContents = library.fs
			.readdirSync(
				`./app/view/pages/admin/content/${dataDashboard.current_pages}`
			)
			.forEach((file) => {
				showPages = `admin/content/${dataDashboard.current_pages}/index`;
			});

		fileJs = library.fs
			.readdirSync(
				`./app/view/pages/admin/content/${dataDashboard.current_pages}`
			)
			.forEach((file) => {
				showJs = `admin/content/${dataDashboard.current_pages}/js`;
			});

		fileCss = library.fs
			.readdirSync(
				`./app/view/pages/admin/content/${dataDashboard.current_pages}`
			)
			.forEach((file) => {
				showCss = `admin/content/${dataDashboard.current_pages}/css`;
			});
	} catch (err) {
		showPages = "admin/no_item";
		showCss = `admin/no_item`;
		showJs = `admin/no_item`;
	}

	if (dataDashboard.current_pages == "profile") {
		dataDashboard.current_id = pages.id;
	} else if (dataDashboard.current_pages == "detail_kecamatan") {
		kecamatan = await library.model.kecamatan.findOne({
			where: {
				id: pages.id,
			},
		});

		dataDashboard.kecamatan = kecamatan;
	} else if (
		dataDashboard.current_pages == "keluarga" ||
		dataDashboard.current_pages == "jadwal_imunisasi"
	) {
		kecamatan = await library.model.kecamatan.findAll({});

		dataDashboard.kecamatan = kecamatan;
	} else if (dataDashboard.current_pages == "detail_puskesmas") {
		puskesmas = await library.model.puskesmas.findOne({
			where: {
				id: pages.id,
			},
		});

		dataDashboard.puskesmas = puskesmas;
	} else if (dataDashboard.current_pages == "imunisasi") {
		nama_anak = await library.model.anggota_keluarga.findAll({
			where: {
				status_keluarga: "2",
			},
		});

		jenis_imun = await library.model.jenis_imunisasi.findAll();

		dataDashboard.nama_anak = nama_anak;
		dataDashboard.jenis_imun = jenis_imun;
	} else if (dataDashboard.current_pages == "detail_kk") {
		kartu_keluarga = await library.model.kartu_keluarga.findOne({
			include: [
				{
					model: library.model.kecamatan,
					as: "kecamatan",
				},
				{
					model: library.model.wilayah_kerja,
					as: "wilayah_kerja",
				},
				{
					model: library.model.puskesmas,
					as: "puskesmas",
				},
			],
			where: {
				id: pages.id,
			},
		});

		dataDashboard.kartu_keluarga = kartu_keluarga;
	} else if (dataDashboard.current_pages == "user_profile") {
		settings = await library.model.setting.findOne({
			where: {
				id_auth: myProfile.id,
			},
		});

		dataDashboard.settings = settings;
	} else if (dataDashboard.current_pages == "dashboard") {
	}

	return res.render("pages/admin/dashboard", {
		csrfToken: req.csrfToken(),
		web_title: "Dashboard - " + req.web_setting.web_name,
		web_desc: req.web_setting.web_desc,
		web_author: req.web_setting.web_author,
		web_keywords: req.web_setting.web_keywords,
		web_url: `${req.web_setting.web_url}/dashboard`,
		web_icon: req.web_setting.web_icon,
		datas: dataDashboard,
		content: function () {
			if (!library.isEmpty(myProfile)) {
				return showPages;
			} else {
				return "admin/no_item";
			}
		},
		includeCss: function () {
			if (!library.isEmpty(myProfile)) {
				return showCss;
			} else {
				return "admin/no_item";
			}
		},
		includeJs: function () {
			if (!library.isEmpty(myProfile)) {
				return showJs;
			} else {
				return "admin/no_item";
			}
		},
	});
};
