const library = require("../core/library");
const { Op } = require("sequelize");

exports.getAll = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.puskesmas.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	id_kecamatan = req.headers["referer"].split("=");

	if (!library.isEmpty(search.value)) {
		const datas = await library.model.puskesmas.findAll({
			where: {
				[library.Op.and]: [
					{
						nama_puskesmas: {
							[library.Op.like]: `%${search.value.toUpperCase()}%`,
						},
					},
					{
						id_kecamatan: id_kecamatan[2],
					},
				],
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.nama_puskesmas}`,
				`${element.status}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.puskesmas.findAll({
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["nama_puskesmas", "ASC"]],
			where: {
				id_kecamatan: id_kecamatan[2],
			},
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.nama_puskesmas}`,
				`${element.status}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.add = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (library.isEmpty(data.nama_puskesmas)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Nama Puskesmas Tidak Boleh Kosong",
				});
			} else {
				const [puskesmas, created] = await library.model.puskesmas.findOrCreate(
					{
						where: {
							nama_puskesmas: data.nama_puskesmas,
						},
						defaults: {
							id: await library.uuid(),
							id_kecamatan: data.id_kecamatan,
							nama_puskesmas: data.nama_puskesmas.trim().toUpperCase(),
							status: 1,
						},
					}
				);

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `<b>${puskesmas.nama_puskesmas}</b> <br>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `<b>${puskesmas.nama_puskesmas}</b> <br>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.deleteById = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));
			id = req.params.id;
			if (library.isEmpty(id)) {
				return res.status(library.responseStatus.OK).json({
					success: false,
					code: library.responseStatus.OK,
					message: `Data Puskesmas Tidak Ditemukan`,
					status: 3,
				});
			} else {
				const puskesmas = await library.model.puskesmas.destroy({
					where: {
						id: id,
					},
				});

				if (puskesmas) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `Puskesmas Berhasil Dihapus`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `Puskesmas <br>Gagal dihapus`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.getById = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));
			id = req.params.id;
			if (library.isEmpty(id)) {
				return res.status(library.responseStatus.OK).json({
					success: false,
					code: library.responseStatus.OK,
					message: `Format Salah!`,
					status: 3,
				});
			} else {
				const puskesmas = await library.model.puskesmas.findAll({
					where: {
						id_kecamatan: id,
					},
				});

				if (puskesmas) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `Data Puskesmas Ditemukan`,
						data: puskesmas,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `Data Puskesmas Tidak di Temukan`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
