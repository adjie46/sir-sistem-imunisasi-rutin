const library = require("../core/library");
const { Op } = require("sequelize");

exports.getAll = async (req, res) => {
	let { draw, start, length, search } = req.query;
	let pageNumber = 1;

	let countData = await library.model.kecamatan.count();
	let array = [];

	if (draw == 1) {
		pageNumber = 1;
	} else {
		pageNumber = 1;
		pageNumber += start++;
	}

	if (!library.isEmpty(search.value)) {
		const datas = await library.model.kecamatan.findAll({
			where: {
				nama_kecamatan: {
					[library.Op.like]: `%${search.value.toUpperCase()}%`,
				},
			},
		});

		countData = datas.length;

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.nama_kecamatan}`,
				`${element.status}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	} else {
		start = req.query.start;

		let datas = await library.model.kecamatan.findAll({
			offset: parseInt(start),
			limit: parseInt(length),
			order: [["nama_kecamatan", "ASC"]],
		});

		await library.asyncForEach(datas, async (element, index) => {
			await library.waitFor(1);

			array.push([
				`${element.id}`,
				`${element.nama_kecamatan}`,
				`${element.status}`,
			]);
		});

		return res.status(200).json({
			draw: draw,
			recordsTotal: countData,
			recordsFiltered: countData,
			data: array,
		});
	}
};

exports.getById = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));
			id = req.params.id;
			if (library.isEmpty(id)) {
				return res.status(library.responseStatus.OK).json({
					success: false,
					code: library.responseStatus.OK,
					message: `Format Salah!`,
					status: 3,
				});
			} else {
				const kecamatan = await library.model.kecamatan.findOne({
					where: {
						id: id,
					},
				});

				if (kecamatan) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `Data Kecataman Ditemukan`,
						data: kecamatan,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `Data Kecamatan Tidak di Temukan`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.deleteById = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));
			id = req.params.id;
			if (library.isEmpty(id)) {
				return res.status(library.responseStatus.OK).json({
					success: false,
					code: library.responseStatus.OK,
					message: `Data OPD Tidak Ditemukan`,
					status: 3,
				});
			} else {
				const kecamatan = await library.model.kecamatan.destroy({
					where: {
						id: id,
					},
				});

				if (kecamatan) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `Kecamatan Berhasil Dihapus`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `Kecamatan <br>Gagal dihapus`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.add = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));

			if (library.isEmpty(data.nama_kecamatan)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Nama Kecamatan Tidak Boleh Kosong",
				});
			} else {
				const [kecamatan, created] = await library.model.kecamatan.findOrCreate(
					{
						where: {
							nama_kecamatan: data.nama_kecamatan,
						},
						defaults: {
							id: await library.uuid(),
							nama_kecamatan: data.nama_kecamatan.trim().toUpperCase(),
							status: 1,
						},
					}
				);

				if (created) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: `<b>${kecamatan.nama_kecamatan}</b> <br>Berhasil Ditambah`,
						status: 3,
					});
				} else {
					return res.status(library.responseStatus.OK).json({
						success: false,
						code: library.responseStatus.OK,
						message: `<b>${kecamatan.nama_kecamatan}</b> <br>Sudah Ada`,
						status: 3,
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.edit = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			data = JSON.parse(JSON.stringify(req.body));
			id = req.params.id;
			if (library.isEmpty(data.nama_kecamatan)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Nama Kecamatan Tidak Boleh Kosong",
				});
			} else {
				uKecamatan = {
					nama_kecamatan: data.nama_kecamatan,
				};

				kecamatan = await library.model.kecamatan.update(uKecamatan, {
					where: {
						id: id,
					},
				});

				if (kecamatan) {
					return res.status(library.responseStatus.OK).json({
						success: true,
						code: library.responseStatus.OK,
						message: "Nama Kecamatan Berhasil Di Update",
					});
				} else {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: "Nama Kecamatan Gagal Di Update",
					});
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.setAction = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			datas = JSON.parse(JSON.stringify(req.body));

			const { id, action } = req.params;

			if (action == "disabled") {
				if (library.isEmpty(id)) {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: "Kecamatan Belum di Pilih",
					});
				} else {
					let status = {
						status: 0,
					};

					const updateStatus = await library.model.kecamatan.update(status, {
						where: {
							id: id,
						},
					});

					if (updateStatus) {
						return res.status(library.responseStatus.OK).json({
							success: true,
							code: library.responseStatus.OK,
							message: "Kecamatan Berhasil Di Non Aktifkan",
						});
					} else {
						return res.status(library.responseStatus.OK).send({
							success: false,
							code: library.responseStatus.OK,
							message: "OPD Belum di Pilih",
						});
					}
				}
			} else if (action == "enabled") {
				if (library.isEmpty(id)) {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: "Kecamatan Belum di Pilih",
					});
				} else {
					let status = {
						status: 1,
					};

					const updateStatus = await library.model.kecamatan.update(status, {
						where: {
							id: id,
						},
					});

					if (updateStatus) {
						return res.status(library.responseStatus.OK).json({
							success: true,
							code: library.responseStatus.OK,
							message: "Kecamatan Berhasil Di Aktifkan",
						});
					} else {
						return res.status(library.responseStatus.OK).send({
							success: false,
							code: library.responseStatus.OK,
							message: "Kecamatan Belum di Pilih",
						});
					}
				}
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
