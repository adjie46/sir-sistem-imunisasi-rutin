//IMPORT LIBRARY
const config = require("../config/config");
const messages = require("./message_response");

const express = require("express");
const cors = require("cors");
const http = require("http");
const bodyParser = require("body-parser");

const hbs = require("hbs");
const hbsutils = require("hbs-utils")(hbs);
const session = require("express-session");
const cookieParser = require("cookie-parser");
const path = require("path");

var csrf = require("csurf");
const sharp = require("sharp");

const crypto = require("crypto");
const algorithm = "aes-256-cbc";
const fs = require("fs");
const jose = require("jose");

const { v5: uuidv5 } = require("uuid");
const model = require("../database/models/index");
const responseStatus = require("./response_status");
const { Op, Sequelize } = require("sequelize");
const axios = require("axios");
const formData = require("./form_data");
const FormData = require("form-data");
const multipart = require("form-data");

const expressip = require("express-ip");
var device = require("express-device");
const moment = require("moment");

const { PDFDocument, StandardFonts, rgb, values } = require("pdf-lib");
const { writeFileSync, readFileSync } = require("fs");
const jwt = require("jsonwebtoken");
var speakeasy = require("speakeasy");

const self = require("node-self");

async function generateSecret() {
	return new Promise(async (resolve, reject) => {
		try {
			const secret = speakeasy.generateSecret({ length: 20 });
			resolve(secret.base32);
		} catch (error) {
			reject(error);
		}
	});
}

async function sendOtp(phone) {
	return new Promise(async (resolve, reject) => {
		secret = await generateSecret();
		const otp = speakeasy.totp({
			secret,
			encoding: "base32",
			digits: 6,
		});

		let messages = `Hi, *Adjie Kurniawan*
Berikut Kode OTP untuk melakukan TTE Dokumen
Kode OTP : *${otp}*

*Mohon jangan berikan kode ini ke sembarang orang!*
Pesan ini dikirim otomatis oleh aplikasi *Signal-TTE* Aceh Tamiang`;

		let wa = await sendWa(phone, messages);

		if (wa.status == "success") {
			resolve(true);
		} else {
			reject(false);
		}
	});
}

async function resizeImage(path, filename, type) {
	return new Promise(async (resolve, reject) => {
		try {
			await sharp(path)
				.toFormat("jpeg", {
					quality: 10,
				})
				.toFile(`public/upload/${type}/small/${filename}`);

			await sharp(path)
				.toFormat("jpeg", {
					quality: 50,
				})
				.toFile(`public/upload/${type}/medium/${filename}`);

			await sharp(path)
				.toFormat("jpeg", {
					quality: 100,
					chromaSubsampling: "4:4:4",
				})
				.toFile(`public/upload/${type}/original/${filename}`);
			resolve(true);
		} catch (error) {
			reject(error);
		}
	});
}

const getPagination = (page, size) => {
	const limit = size ? +size : 10;
	const offset = page ? page * limit : 0;

	return { limit, offset };
};

async function addFooter(filePdf, fileName) {
	const document = await PDFDocument.load(readFileSync(filePdf));

	const courierBoldFont = await document.embedFont(StandardFonts.Courier);
	const firstPage = document.getPages();

	const imgBuffer = fs.readFileSync("./app/config/footer.jpg");
	const img = await document.embedJpg(imgBuffer);

	firstPage.forEach(async (page) => {
		await page.drawImage(img, {
			x: 0,
			y: 10,
			width: img.width / 4 - 10,
			height: img.height / 4,
		});
	});

	writeFileSync("./public/upload/temp/" + fileName, await document.save());
}

async function addVisual(filePdf, anchor) {
	return new Promise(async (resolve, reject) => {
		let data = new multipart();
		data.append("file", fs.createReadStream(filePdf));
		data.append("anchor", anchor);

		let configs = {
			method: "post",
			maxBodyLength: Infinity,
			url: config.urlAnchor,
			headers: {
				...data.getHeaders(),
			},
			data: data,
		};

		axios
			.request(configs)
			.then((response) => {
				resolve(response.data);
			})
			.catch((error) => {
				reject(error);
			});
	});
}

async function addSampleTTE(filePdf, fileName, coordinate) {
	const document = await PDFDocument.load(readFileSync(filePdf));

	const firstPage = document.getPages();

	const imgBuffer = fs.readFileSync("./app/config/sample_visual_tte.png");
	const img = await document.embedPng(imgBuffer);

	var List = coordinate;

	await asyncForEach(List, async (element, index) => {
		await waitFor(1);
		firstPage[parseInt(element.page - 1)].drawImage(img, {
			x: element.x - 10,
			y: element.y,
			width: 246,
			height: 55,
		});
	});

	writeFileSync("./public/upload/temp/" + fileName, await document.save());
}

async function mergerQR(filePdf, fileName, coordinate, fileParaf) {
	return new Promise(async (resolve, reject) => {
		try {
			console.log("KITA COBA");
			await sharp(`./public/upload/temp/test.png`)
				.flatten({
					background: "#FFFFFF",
				})
				.resize(1190, 274, {
					kernel: sharp.kernel.nearest,
					fit: "contain",
					position: "right top",
					background: {
						r: 255,
						g: 255,
						b: 255,
						alpha: 0,
					},
				})
				.toFile(`./public/upload/temp/tempQR.png`)
				.then(() => {
					sharp(
						"./public/upload/fileTTE/e6684606-b95c-49e6-8ee9-df035fe0c6e6.png"
					)
						.flatten({
							background: "#FFFFFF",
						})
						/* .sharpen()
					.withMetadata() */
						.png({
							quality: 70,
						})
						.composite([
							{
								input: `./public/upload/temp/tempQR.png`,
								left: 0,
								top: 0,
							},
						])
						/* .toBuffer()
						.then(async function (outputBuffer) {
							resolve(outputBuffer);
						}) */
						.toFile("./public/upload/temp" + `/finalTTE.png`);
				});
		} catch (error) {
			console.log(error);
			reject(error);
		}
	});
}

async function addTTE(filePdf, fileName, coordinate, fileParaf) {
	const document = await PDFDocument.load(readFileSync(filePdf));

	const firstPage = document.getPages();

	const imgBuffer = fs.readFileSync(fileParaf);
	const img = await document.embedPng(imgBuffer);

	var List = coordinate;

	await asyncForEach(List, async (element, index) => {
		await waitFor(1);
		firstPage[parseInt(element.page - 1)].drawImage(img, {
			x: element.x + 110,
			y: element.y + 15,
			width: 246,
			height: 55,
		});
	});

	writeFileSync(
		"./public/upload/temp/TEMP_TTE_" + fileName,
		await document.save()
	);

	return "./public/upload/temp/TEMP_TTE_" + fileName;
}

async function addParaf(filePdf, fileName, coordinate, fileParaf) {
	const document = await PDFDocument.load(readFileSync(filePdf));

	const firstPage = document.getPages();

	const imgBuffer = fs.readFileSync(fileParaf);
	const img = await document.embedJpg(imgBuffer);

	var List = coordinate;

	await asyncForEach(List, async (element, index) => {
		await waitFor(1);
		firstPage[parseInt(element.page - 1)].drawImage(img, {
			x: element.x + 110,
			y: element.y + 15,
			width: 30,
			height: 30,
		});
	});

	writeFileSync(
		"./public/upload/temp/TEMP_PARAF_" + fileName,
		await document.save()
	);

	return "./public/upload/temp/TEMP_PARAF_" + fileName;
}

async function deleteFile(filename) {
	return new Promise(async (resolve, reject) => {
		try {
			if (fs.existsSync(`./public/upload/temp/${filename}`)) {
				fs.unlinkSync(`./public/upload/temp/${filename}`);
			}
		} catch (error) {
			reject(error);
		}
	});
}

async function prayer_time() {
	return new Promise(async (resolve, reject) => {
		const today = new Date();
		const month = today.getMonth() + 1;
		const years = today.getFullYear();
		try {
			let config = {
				method: "get",
				maxBodyLength: Infinity,
				url: `https://muslimummah.co/api-server/prayer/monthly-time/v1?location=Kabupaten Aceh Tamiang&month[]=${years}-${month}`,
				headers: {},
			};

			axios
				.request(config)
				.then((response) => {
					resolve(
						JSON.stringify(
							response.data.data.prayer_time_monthly[0].prayer_day_list
						)
					);
				})
				.catch((error) => {
					reject(error);
				});
		} catch (error) {
			reject(error);
		}
	});
}

function currentDate(date) {
	date = new Date();

	const year = date.getFullYear();
	const month = String(date.getMonth() + 1).padStart(2, "0");
	const day = String(date.getDate()).padStart(2, "0");

	return `${year}-${month}-${day}`;
}

function currentTime() {
	const ct = new Date();
	const hours = ct.getHours();
	const minutes = ct.getMinutes();
	const seconds = ct.getSeconds();

	const formattedTime = `${hours}:${minutes}`;
	return formattedTime;
}

function convertDate(date, format, to) {
	const originalDateString = date; // Assuming "date[0]" is your original date string
	const isoFormattedDate = moment(originalDateString, format).format(to);
	const fDate = moment(isoFormattedDate, to);
	return fDate;
}

function moveFile(source, destination) {
	fs.copyFile(source, destination, (err) => {
		if (err) {
			console.error("Error moving the file:", err);
		} else {
			console.log("File moved successfully!");
		}
	});

	try {
		if (fs.existsSync(`${source}`)) {
			fs.unlinkSync(`${source}`);
		}
	} catch (error) {
		reject(error);
	}
}

async function getStatusTTE(nik) {
	return new Promise(async (resolve, reject) => {
		try {
			promise = await Promise.all([
				apiBsre(nik, "user/status/"),
				apiBsre(nik, "user/profile/"),
			])
				.then((data) => {
					resolve(data);
				})
				.catch((err) => {
					reject(err);
				});
		} catch (error) {
			reject(error);
		}
	});
}

async function paraf(datas) {
	return new Promise(async (resolve, reject) => {
		try {
			var filesss = datas.FileAsli;
			var imageParaf = datas.visual_tte;

			var username, password, urlTte;

			var data = new multipart();

			if (config.mode == "DEV") {
				username = config.usernameSign_dev;
				password = config.passwordSign_dev;
				urlTte = config.apiSignDev;

				data.append("nik", "0803202100007062");
				data.append("passphrase", "!Bsre1221*");
				data.append("tampilan", "invisible");
				data.append("tag_koordinat", datas.tag_koordinat);
				data.append("file", fs.createReadStream(filesss), "file.pdf");
				data.append("imageTTD", fs.createReadStream(imageParaf), "paraf.png");
			} else if (config.mode == "PROD") {
				username = config.usernameSign_prod;
				password = config.passwordSign_prod;
				urlTte = config.apiSignProd;

				data.append("nik", datas.nik);
				data.append("passphrase", datas.passphrase);
				data.append("tampilan", "invisible");
				data.append("tag_koordinat", datas.tag_koordinat);
				data.append("file", fs.createReadStream(filesss), "file.pdf");
			}

			var configs = {
				method: "post",
				url: urlTte,
				auth: {
					username: username,
					password: password,
				},
				headers: {
					...data.getHeaders(),
				},
				data: data,
				responseType: "arraybuffer",
				//responseType: "json",
			};

			axios(configs)
				.then(async function (response) {
					await fs.promises.writeFile(
						`./public/upload/pdf/FILE_PARAF_${datas.index_timeline}_${datas.namaFile}`,
						response.data,
						(err) => {
							if (err) {
								reject("Server BSre Bermasalah!");
							} else {
								console.log("The file has been saved!");
								resolve(
									`./public/upload/pdf/FILE_PARAF_${datas.index_timeline}_${datas.namaFile}`
								);
							}
						}
					);

					resolve(
						`/assets/upload/pdf/FILE_PARAF_${datas.index_timeline}_${datas.namaFile}`
					);
				})
				.catch(function (error) {
					console.log(error);
					reject(error);
				});
		} catch (error) {
			reject(error);
		}
	});
}

async function tte(datas) {
	return new Promise(async (resolve, reject) => {
		try {
			var filesss = datas.FileAsli;
			var imageParaf = datas.visual_tte;

			var username, password, urlTte;

			var data = new multipart();

			if (config.mode == "DEV") {
				username = config.usernameSign_dev;
				password = config.passwordSign_dev;
				urlTte = config.apiSignDev;

				data.append("nik", "0803202100007062");
				data.append("passphrase", "!Bsre1221*");
				data.append("tampilan", "invisible");
				data.append("tag_koordinat", datas.tag_koordinat);
				data.append("file", fs.createReadStream(filesss), "file.pdf");
				data.append("imageTTD", fs.createReadStream(imageParaf), "paraf.png");
			} else if (config.mode == "PROD") {
				username = config.usernameSign_prod;
				password = config.passwordSign_prod;
				urlTte = config.apiSignProd;

				data.append("nik", datas.nik);
				data.append("passphrase", datas.passphrase);
				data.append("tampilan", "invisible");
				data.append("tag_koordinat", datas.tag_koordinat);
				data.append("file", fs.createReadStream(filesss), "file.pdf");
			}

			var configs = {
				method: "post",
				url: urlTte,
				auth: {
					username: username,
					password: password,
				},
				headers: {
					...data.getHeaders(),
				},
				data: data,
				responseType: "arraybuffer",
				//responseType: "json",
			};

			axios(configs)
				.then(async function (response) {
					await fs.promises.writeFile(
						`./public/upload/pdf/FILE_TTE_${datas.index_timeline}_${datas.namaFile}`,
						response.data,
						(err) => {
							if (err) {
								reject("Server BSre Bermasalah!");
							} else {
								console.log("The file has been saved!");
								resolve(
									`./public/upload/pdf/FILE_TTE_${datas.index_timeline}_${datas.namaFile}`
								);
							}
						}
					);

					resolve(
						`/assets/upload/pdf/FILE_PARAF_${datas.index_timeline}_${datas.namaFile}`
					);
				})
				.catch(function (error) {
					console.log(error);
					reject(error);
				});
		} catch (error) {
			reject(error);
		}
	});
}

async function apiBsre(nik, action) {
	return new Promise(async (resolve, reject) => {
		try {
			var username, password, urlTte;

			urlTte = config.baseUrlApiSignProd;
			username = config.usernameSign_prod;
			password = config.passwordSign_prod;

			var configs = {
				method: "get",
				url: urlTte + action + nik,
				auth: {
					username: username,
					password: password,
				},
				responseType: "json",
			};

			axios(configs)
				.then(async function (response) {
					resolve(response.data);
				})
				.catch(function (error) {
					reject(error);
				});
		} catch (error) {
			reject(error);
		}
	});
}

async function sendWa(message, no_hp) {
	return new Promise(async (resolve, reject) => {
		try {
			let data = JSON.stringify({
				number: "62" + no_hp,
				type: "text",
				message: message,
				instance_id: config.instanceId,
				access_token: config.accessToken,
			});

			let configs = {
				method: "post",
				maxBodyLength: Infinity,
				url: "https://bot.adjiekurniawan.my.id/api/send",
				headers: {
					"Content-Type": "application/json",
				},
				data: data,
			};

			axios
				.request(configs)
				.then((response) => {
					resolve(response);
				})
				.catch((error) => {
					reject(error);
				});
		} catch (error) {
			reject(error);
		}
	});
}

//Encrypting text
async function encrypt(text) {
	return new Promise(async (resolve, reject) => {
		try {
			let iv = crypto.randomBytes(16);
			let salt = crypto.randomBytes(16);
			let key = crypto.scryptSync(config.jwtSecret, salt, 32);

			let cipher = crypto.createCipheriv(algorithm, key, iv);
			let encrypted = cipher.update(text, "utf8", "hex");
			encrypted += cipher.final("hex");
			resolve(`${iv.toString("hex")}:${salt.toString("hex")}:${encrypted}`);
		} catch (error) {
			reject(error);
		}
	});
}

async function test(encryptedText) {
	const algorithm = "aes-128-cbc"; // AES with 128-bit key
	const keyHex = "6d797030614278446469346d6d506c50"; // 16 bytes key in hexadecimal format
	const ivHex = "533378696c526c75554b6e72384e3571"; // 16 bytes IV in hexadecimal format

	const key = Buffer.from(keyHex, "hex");
	const iv = Buffer.from(ivHex, "hex");

	const decipher = crypto.createDecipheriv(algorithm, key, iv);
	let decrypted = decipher.update(encryptedText, "base64", "utf-8");
	decrypted += decipher.final("utf-8");
	return decrypted;
}

async function test1(text) {
	const algorithm = "aes-128-cbc";
	const keyHex = "6d797030614278446469346d6d506c50"; // 16 bytes key in hexadecimal format
	const ivHex = "533378696c526c75554b6e72384e3571"; // 16 bytes IV in hexadecimal format

	const key = Buffer.from(keyHex, "hex");
	const iv = Buffer.from(ivHex, "hex");

	const cipher = crypto.createCipheriv(algorithm, key, iv);
	let encrypted = cipher.update(text, "utf-8", "base64");
	encrypted += cipher.final("base64");
	return encrypted;
}

const waitFor = (ms) => new Promise((r) => setTimeout(r, ms));

async function asyncForEach(array, callback) {
	for (let index = 0; index < array.length; index++) {
		await callback(array[index], index, array);
	}
}

// Decrypting text
async function decrypt(text) {
	return new Promise(async (resolve, reject) => {
		try {
			let [ivs, salts, data] = text.split(":");
			let iv = Buffer.from(ivs, "hex");
			let salt = Buffer.from(salts, "hex");
			let key = crypto.scryptSync(config.jwtSecret, salt, 32);

			let decipher = crypto.createDecipheriv(algorithm, key, iv);
			let decrypted = decipher.update(data, "hex", "utf8");
			decrypted += decipher.final("utf8");
			resolve(decrypted.toString());
		} catch (error) {
			reject(error);
		}
	});
}

const encryptedJwt = (subject, payload, secret, expired) => {
	return new jose.EncryptJWT(payload)
		.setProtectedHeader({
			alg: "dir",
			enc: "A256GCM",
		})
		.setIssuedAt()
		.setSubject(subject)
		.setIssuer(config.baseUrl)
		.setAudience(config.baseUrl)
		.setExpirationTime(expired)
		.encrypt(secret);
};

const decryptJwt = async (jwt, secret) => {
	const options = {
		issuer: config.baseUrl,
		audience: config.baseUrl,
		contentEncryptionAlgorithms: ["A256GCM"],
		keyManagementAlgorithms: ["dir"],
	};
	return jose.jwtDecrypt(jwt, secret, options);
};

const logs = async (data) => {
	newLogs = {
		id_auth: data.id_auth,
		log_type: data.log_type,
		log_action: data.log_action,
		log_os: data.log_os,
		log_devices: data.log_devices,
		log_ip: data.log_ip,
	};

	await model.log.create(newLogs);
};

const uuid = async () => {
	return new Promise(async (resolve, reject) => {
		try {
			uuidGen = uuidv5(await randomString(10), config.nameSpace);
			resolve(uuidGen);
		} catch (error) {
			reject(error);
		}
	});
};

const fileName = async (length) => {
	return new Promise(async (resolve, reject) => {
		try {
			uuidGen = uuidv5(await randomString(length), config.nameSpace);
			const md5Hash = crypto.createHash("md5").update(uuidGen).digest("hex");
			resolve(md5Hash);
		} catch (error) {
			reject(error);
		}
	});
};

//Random String
async function randomString(length) {
	return new Promise(async (resolve, reject) => {
		try {
			var result = "";
			var characters =
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var charactersLength = characters.length;
			for (var i = 0; i < length; i++) {
				result += characters.charAt(
					Math.floor(Math.random() * charactersLength)
				);
			}
			resolve(result);
		} catch (error) {
			reject(error);
		}
	});
}

//API_KEY
async function apiKeyGenerator(separator, keySegments, segmentLength) {
	return new Promise(async (resolve, reject) => {
		try {
			const bytes = crypto.randomBytes(keySegments * segmentLength);
			const segments = [];

			for (let i = 0; i < keySegments; i++) {
				const start = i * segmentLength;
				const end = start + segmentLength;
				segments.push(bytes.slice(start, end).toString("hex").toUpperCase());
			}

			resolve(segments.join(separator));
		} catch (error) {
			reject(error);
		}
	});
}

function isEmpty(obj) {
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop)) return false;
	}
	return true;
}

function truncate(str, n) {
	if (str.length > n) {
		return str.substring(0, n) + "******";
	} else {
		return str;
	}
}

function padTo2Digits(num) {
	return num.toString().padStart(2, "0");
}

function convertMsToTime(milliseconds) {
	let seconds = Math.floor(milliseconds / 1000);
	let minutes = Math.floor(seconds / 60);
	let hours = Math.floor(minutes / 60);

	seconds = seconds % 60;
	minutes = minutes % 60;

	// 👇️ If you don't want to roll hours over, e.g. 24 to 00
	// 👇️ comment (or remove) the line below
	// commenting next line gets you `24:00:00` instead of `00:00:00`
	// or `36:15:31` instead of `12:15:31`, etc.
	hours = hours % 24;

	return `${padTo2Digits(hours)}:${padTo2Digits(minutes)}:${padTo2Digits(
		seconds
	)}`;
}

function toRupiah(angka, prefix) {
	let angka1 = angka.toString();
	let number_string = angka1.replace(/[^,\d]/g, "").toString();
	var split = number_string.split(",");
	var sisa = split[0].length % 3;
	var rupiah = split[0].substr(0, sisa);
	var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

	if (ribuan) {
		separator = sisa ? "." : "";
		rupiah += separator + ribuan.join(".");
	}

	rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
	return prefix == undefined ? rupiah : rupiah ? "Rp " + rupiah : "";
}

const getPagingData = (data, page, limit) => {
	const { count: totalItems, rows: datas } = data;
	const currentPage = page ? +page : 0;
	const totalPages = Math.ceil(totalItems / limit);

	return { totalItems, datas, totalPages, currentPage };
};

async function hitungUmur(tanggalLahir) {
	// Split the date string into day, month, and year
	const dateParts = tanggalLahir.split("-");
	const day = parseInt(dateParts[0], 10);
	const month = parseInt(dateParts[1], 10);
	const year = parseInt(dateParts[2], 10);

	// Create a Date object with the parsed values
	const dob = new Date(year, month - 1, day); // Month is 0-based, so subtract 1

	// Check if the date is valid
	if (isNaN(dob.getTime())) {
		return { tahun: 0, bulan: 0, hari: 0 };
	}

	const today = new Date();

	let selisihTahun = today.getFullYear() - dob.getFullYear();
	let selisihBulan = today.getMonth() - dob.getMonth();
	let selisihHari = today.getDate() - dob.getDate();

	if (selisihHari < 0) {
		selisihBulan--;
		const lastDayOfLastMonth = new Date(
			today.getFullYear(),
			today.getMonth(),
			0
		).getDate();
		selisihHari += lastDayOfLastMonth;
	}

	if (selisihBulan < 0) {
		selisihTahun--;
		selisihBulan += 12;
	}

	return {
		tahun: selisihTahun,
		bulan: selisihBulan,
		hari: selisihHari,
	};
}

function removeDuplicates(array) {
	const uniqueItems = {};
	const result = [];

	for (const item of array) {
		const stringifiedItem = JSON.stringify(item);
		if (!uniqueItems[stringifiedItem]) {
			result.push(item);
			uniqueItems[stringifiedItem] = true;
		}
	}

	return result;
}

function removeDuplicatesByKey(array, key) {
	const uniqueItems = {};
	const result = [];

	for (const item of array) {
		const keyValue = item[key];
		if (!uniqueItems[keyValue]) {
			result.push(item);
			uniqueItems[keyValue] = true;
		}
	}

	return result;
}

function mergeData(data) {
	const mergedData = {};

	data.forEach((item) => {
		const key = `${item.nama_ibu}_${item.nama_anak}`;
		if (!mergedData[key]) {
			mergedData[key] = { ...item };
		} else {
			mergedData[key].jenis_imun.push(...item.jenis_imun);
		}
	});

	// Convert the mergedData object back to an array
	const mergedArray = Object.values(mergedData);
	return mergedArray;
}

module.exports = {
	config,
	express,
	cors,
	http,
	bodyParser,
	hbs,
	hbsutils,
	session,
	cookieParser,
	path,
	csrf,
	messages,
	resizeImage,
	encrypt,
	decrypt,
	encryptedJwt,
	decryptJwt,
	randomString,
	isEmpty,
	toRupiah,
	waitFor,
	asyncForEach,
	uuid,
	model,
	responseStatus,
	Op,
	axios,
	formData,
	logs,
	expressip,
	device,
	fs,
	prayer_time,
	currentDate,
	getPagination,
	getPagingData,
	truncate,
	getStatusTTE,
	apiBsre,
	sharp,
	convertMsToTime,
	moment,
	Sequelize,
	addFooter,
	addVisual,
	addSampleTTE,
	deleteFile,
	moveFile,
	apiKeyGenerator,
	test,
	test1,
	jwt,
	jose,
	sendOtp,
	fileName,
	paraf,
	addParaf,
	addTTE,
	tte,
	mergerQR,
	convertDate,
	hitungUmur,
	currentTime,
	sendWa,
	removeDuplicates,
	removeDuplicatesByKey,
	mergeData,
};
