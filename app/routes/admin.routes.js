const express = require("express");
const { web_setting } = require("../middleware/web_setting");
const route = express.Router();

const webSetting = require("../middleware/web_setting");
const authMiddleware = require("../middleware/auth.middleware");

const errorController = require("../controller/error.controller");
const authController = require("../controller/auth.controller");
const dashboardController = require("../controller/dashboard.controller");
const userController = require("../controller/user.controller");
const settingController = require("../controller/setting.controller");

const kecamatanController = require("../controller/kecamatan.controller");
const puskesmasController = require("../controller/puskesmas.controller");
const wKerjaController = require("../controller/wilayah_kerja.controller");
const kkController = require("../controller/kartu_keluarga.controller");
const imunisasiController = require("../controller/imunisasi.controller");
const jadwalImunisasiController = require("../controller/jadwal_imunisasi.controller");

route.get(
	"/404",
	authMiddleware.mode,
	webSetting.web_setting,
	errorController.err_404
);

route.get(
	"/maintenance",
	authMiddleware.checkMaintenance,
	webSetting.web_setting,
	errorController.maintenance
);

route.get(
	"/login",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.auth,
	authController.login_pages
);
route.get(
	"/",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.auth,
	authController.login_pages
);

route.get(
	"/dashboard",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	dashboardController.dashboard_pages
);

//AUTH
route.post(
	"/login",
	authMiddleware.mode,
	webSetting.web_setting,
	authController.login_action
);

route.get(
	"/logout",
	authMiddleware.mode,
	webSetting.web_setting,
	authController.authLogout
);

//USER
route.get(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

route.post(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

route.put(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

route.delete(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

//SETTING
route.get(
	"/setting/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.setting
);

route.post(
	"/setting/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.add
);

route.delete(
	"/setting/:action/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.delete
);

route.put(
	"/setting/:action/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.copy
);

route.post(
	"/profile/change_photo",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.change_photo
);

//KECAMATAN
route.get(
	"/kecamatan",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kecamatanController.getAll
);

route.post(
	"/kecamatan",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kecamatanController.add
);

route.get(
	"/kecamatan/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kecamatanController.getById
);

route.put(
	"/kecamatan/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kecamatanController.edit
);

route.delete(
	"/kecamatan/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kecamatanController.deleteById
);

route.post(
	"/kecamatan/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kecamatanController.setAction
);

//PUSKESMAS
route.get(
	"/puskesmas",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	puskesmasController.getAll
);

route.post(
	"/puskesmas",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	puskesmasController.add
);

route.delete(
	"/puskesmas/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	puskesmasController.deleteById
);

route.get(
	"/puskesmas/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	puskesmasController.getById
);

//WILAYAH KERJA
route.get(
	"/wilayah_kerja",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	wKerjaController.getAll
);

route.post(
	"/wilayah_kerja",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	wKerjaController.add
);

route.delete(
	"/wilayah_kerja/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	wKerjaController.deleteById
);

route.get(
	"/wilayah_kerja/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	wKerjaController.getById
);

//KARTU KELUARGA
route.get(
	"/kartu_keluarga",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kkController.getAll
);

route.post(
	"/kartu_keluarga",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kkController.add
);

//ANGGOTA KELUARGA
route.get(
	"/anggota_keluarga",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kkController.getAnggotaKeluarga
);

route.post(
	"/anggota_keluarga",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	kkController.addAnggotaKeluarga
);

//JENIS IMUNISASI
route.get(
	"/jenis_imunisasi",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	imunisasiController.getAll
);

route.post(
	"/jenis_imunisasi",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	imunisasiController.add
);

//JADWAL IMUNISASI
route.get(
	"/jadwal_imunisasi",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	jadwalImunisasiController.getAll
);

route.get("/send_notif", jadwalImunisasiController.sendNotif);
route.get("/send_wa", jadwalImunisasiController.sendWa);

route.post(
	"/jadwal_imunisasi",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	jadwalImunisasiController.add
);

//IMUNISASI
route.get(
	"/data_imunisasi",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	imunisasiController.getDataImunisasi
);

route.post(
	"/data_imunisasi",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	imunisasiController.addImunisasi
);

route.all(
	"*",
	authMiddleware.mode,
	webSetting.web_setting,
	errorController.err_404
);

module.exports = route;
