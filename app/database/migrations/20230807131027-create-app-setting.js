"use strict";
/** @type {import('sequelize-cli').Migration} */
var dataType = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("app_settings", {
			id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.UUID,
				defaultValue: dataType.UUID,
			},
			app_name: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			app_package: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			app_type: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			app_token: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("app_settings");
	},
};
