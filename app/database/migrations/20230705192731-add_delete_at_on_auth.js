"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.addColumn(
				"auths", // table name
				"deletedAt", // new field name
				{
					type: "DATETIME",
					allowNull: true,
				}
			),
		]);
	},

	async down(queryInterface, Sequelize) {
		return Promise.all([queryInterface.removeColumn("auths", "deletedAt")]);
	},
};
