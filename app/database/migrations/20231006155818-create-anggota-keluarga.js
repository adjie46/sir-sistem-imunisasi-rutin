"use strict";
/** @type {import('sequelize-cli').Migration} */
var DataTypes = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("anggota_keluargas", {
			id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.UUID,
				defaultValue: DataTypes.UUIDV4,
			},
			id_kk: {
				allowNull: false,
				type: Sequelize.UUID,
				foreignKey: true,
				allowNull: true,
				references: {
					model: "kartu_keluargas",
					key: "id",
				},
			},
			full_name: {
				allowNull: false,
				type: Sequelize.STRING,
			},
			status_keluarga: {
				allowNull: false,
				type: Sequelize.STRING,
			},
			kepala_keluarga: {
				allowNull: false,
				type: Sequelize.BOOLEAN,
			},
			anak_ke: {
				allowNull: false,
				type: Sequelize.STRING,
			},
			is_anak: {
				allowNull: false,
				type: Sequelize.BOOLEAN,
			},
			tgl_lahir: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("anggota_keluargas");
	},
};
