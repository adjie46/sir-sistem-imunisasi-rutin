"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.addColumn(
				"anggota_keluargas", // table name
				"nik", // new field name
				{
					type: Sequelize.STRING,
					allowNull: false,
					after: "id",
				}
			),
			queryInterface.addColumn(
				"anggota_keluargas", // table name
				"tempat_lahir", // new field name
				{
					type: Sequelize.STRING,
					allowNull: false,
					after: "tgl_lahir",
				}
			),
		]);
	},

	async down(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.removeColumn("anggota_keluargas", "nik"),
			queryInterface.removeColumn("anggota_keluargas", "tempat_lahir"),
		]);
	},
};
