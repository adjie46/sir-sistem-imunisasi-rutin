"use strict";
/** @type {import('sequelize-cli').Migration} */
var DataTypes = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("puskesmas", {
			id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.UUID,
				defaultValue: DataTypes.UUIDV4,
			},
			nama_puskesmas: {
				allowNull: false,
				type: Sequelize.STRING,
			},
			status: {
				allowNull: true,
				type: Sequelize.BOOLEAN,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("puskesmas");
	},
};
