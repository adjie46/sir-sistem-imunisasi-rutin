"use strict";
/** @type {import('sequelize-cli').Migration} */
var dataType = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("auth_devices", {
			id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.UUID,
				defaultValue: dataType.UUID,
			},
			id_auth: {
				type: Sequelize.UUID,
				defaultValue: dataType.UUID,
				allowNull: false,
				references: {
					model: "auths",
					id: "id",
				},
				onUpdate: "CASCADE",
				onDelete: "CASCADE",
			},
			imei_device: {
				type: Sequelize.STRING,
			},
			hardware: {
				type: Sequelize.STRING,
			},
			manufacturer: {
				type: Sequelize.STRING,
			},
			type: {
				type: Sequelize.STRING,
			},
			os_name: {
				type: Sequelize.STRING,
			},
			status: {
				type: Sequelize.STRING,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("auth_devices");
	},
};
