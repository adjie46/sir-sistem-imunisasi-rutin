"use strict";
/** @type {import('sequelize-cli').Migration} */
var DataTypes = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("jadwal_imunisasis", {
			id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.UUID,
				defaultValue: DataTypes.UUIDV4,
			},
			tgl_imunisasi: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			id_kecamatan: {
				allowNull: false,
				type: Sequelize.UUID,
				foreignKey: true,
				allowNull: true,
				references: {
					model: "kecamatans",
					key: "id",
				},
			},
			id_puskesmas: {
				allowNull: false,
				type: Sequelize.UUID,
				foreignKey: true,
				allowNull: true,
				references: {
					model: "puskesmas",
					key: "id",
				},
			},
			id_wilayah_kerja: {
				allowNull: false,
				type: Sequelize.UUID,
				foreignKey: true,
				allowNull: true,
				references: {
					model: "wilayah_kerjas",
					key: "id",
				},
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("jadwal_imunisasis");
	},
};
