"use strict";
/** @type {import('sequelize-cli').Migration} */
var dataType = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("settings", {
			id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.UUID,
				defaultValue: dataType.UUID,
			},
			id_auth: {
				type: Sequelize.UUID,
				defaultValue: dataType.UUID,
				allowNull: false,
				references: {
					model: "auths",
					id: "id",
				},
				onUpdate: "CASCADE",
				onDelete: "CASCADE",
			},
			notification: {
				type: Sequelize.BOOLEAN,
			},
			notif_wa: {
				type: Sequelize.BOOLEAN,
			},
			notif_email: {
				type: Sequelize.BOOLEAN,
			},
			twofa: {
				type: Sequelize.BOOLEAN,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("settings");
	},
};
