"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.addColumn(
				"puskesmas", // table name
				"id_kecamatan", // new field name
				{
					type: Sequelize.UUID,
					foreignKey: true,
					allowNull: true,
					references: {
						model: "kecamatans",
						key: "id",
					},
					after: "id",
				}
			),
		]);
	},

	async down(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.removeColumn("puskesmas", "id_kecamatan"),
		]);
	},
};
