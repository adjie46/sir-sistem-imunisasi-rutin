"use strict";
/** @type {import('sequelize-cli').Migration} */
var DataTypes = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("anak_has_imuns", {
			id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.UUID,
				defaultValue: DataTypes.UUIDV4,
			},
			id_anak: {
				type: Sequelize.UUID,
				foreignKey: true,
				allowNull: false,
				references: {
					model: "anggota_keluargas",
					key: "id",
				},
			},
			id_imun: {
				type: Sequelize.UUID,
				foreignKey: true,
				allowNull: false,
				references: {
					model: "jenis_imunisasis",
					key: "id",
				},
			},
			tgl_imun: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("anak_has_imuns");
	},
};
