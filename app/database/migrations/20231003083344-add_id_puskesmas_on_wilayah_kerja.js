"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.addColumn(
				"wilayah_kerjas", // table name
				"id_puskesmas", // new field name
				{
					type: Sequelize.UUID,
					foreignKey: true,
					allowNull: true,
					references: {
						model: "puskesmas",
						key: "id",
					},
					after: "id",
				}
			),
		]);
	},

	async down(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.removeColumn("wilayah_kerja", "id_puskesmas"),
		]);
	},
};
