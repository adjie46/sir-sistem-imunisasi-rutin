"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.addColumn(
				"anggota_keluargas", // table name
				"no_hp", // new field name
				{
					type: Sequelize.STRING,
					allowNull: false,
					after: "tempat_lahir",
				}
			),
		]);
	},

	async down(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.removeColumn("anggota_keluargas", "no_hp"),
		]);
	},
};
