"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class kartu_keluarga extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			kartu_keluarga.belongsTo(models.kecamatan, {
				foreignKey: "id_kecamatan",
				targetKey: "id",
				sourceKey: "id_kecamatan",
				as: "kecamatan",
			});
			kartu_keluarga.belongsTo(models.puskesmas, {
				foreignKey: "id_puskesmas",
				targetKey: "id",
				sourceKey: "id_puskesmas",
				as: "puskesmas",
			});
			kartu_keluarga.belongsTo(models.wilayah_kerja, {
				foreignKey: "id_wilayah_kerja",
				targetKey: "id",
				sourceKey: "id_wilayah_kerja",
				as: "wilayah_kerja",
			});
			kartu_keluarga.hasMany(models.anggota_keluarga, {
				foreignKey: "id_kk",
				targetKey: "id_kk",
				sourceKey: "id",
				as: "anggota_keluarga",
			});
		}
	}
	kartu_keluarga.init(
		{
			no_kk: DataTypes.STRING,
			id_kecamatan: DataTypes.STRING,
			id_puskesmas: DataTypes.STRING,
			id_wilayah_kerja: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "kartu_keluarga",
		}
	);
	return kartu_keluarga;
};
