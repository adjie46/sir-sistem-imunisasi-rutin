"use strict";
const { Model } = require("sequelize");
const moment = require("moment");
module.exports = (sequelize, DataTypes) => {
	class anggota_keluarga extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	anggota_keluarga.init(
		{
			nik: DataTypes.STRING,
			id_kk: DataTypes.STRING,
			full_name: DataTypes.STRING,
			status_keluarga: DataTypes.STRING,
			kepala_keluarga: DataTypes.BOOLEAN,
			anak_ke: DataTypes.STRING,
			is_anak: DataTypes.BOOLEAN,
			tgl_lahir: DataTypes.DATE,
			tempat_lahir: DataTypes.STRING,
			no_hp: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "anggota_keluarga",
			getterMethods: {
				tgl_lahir() {
					var date;
					date = moment(this.getDataValue("tgl_lahir")).format("DD-MM-YYYY");
					if (date == "Invalid date") {
						date = null;
					}
					return date;
				},
			},
		}
	);
	return anggota_keluarga;
};
