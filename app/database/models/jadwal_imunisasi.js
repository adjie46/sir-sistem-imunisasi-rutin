"use strict";
const { Model } = require("sequelize");
const moment = require("moment");
module.exports = (sequelize, DataTypes) => {
	class jadwal_imunisasi extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			jadwal_imunisasi.belongsTo(models.kecamatan, {
				foreignKey: "id_kecamatan",
				targetKey: "id",
				sourceKey: "id_kecamatan",
				as: "kecamatan",
			});
			jadwal_imunisasi.belongsTo(models.puskesmas, {
				foreignKey: "id_puskesmas",
				targetKey: "id",
				sourceKey: "id_puskesmas",
				as: "puskesmas",
			});
			jadwal_imunisasi.belongsTo(models.wilayah_kerja, {
				foreignKey: "id_wilayah_kerja",
				targetKey: "id",
				sourceKey: "id_wilayah_kerja",
				as: "wilayah_kerja",
			});
		}
	}
	jadwal_imunisasi.init(
		{
			tgl_imunisasi: DataTypes.DATE,
			id_kecamatan: DataTypes.STRING,
			id_puskesmas: DataTypes.STRING,
			id_wilayah_kerja: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "jadwal_imunisasi",
			getterMethods: {
				tgl_imunisasi_format() {
					var date;
					date = moment(this.getDataValue("tgl_imunisasi")).format(
						"DD-MM-YYYY"
					);
					if (date == "Invalid date") {
						date = null;
					}
					return date;
				},
			},
		}
	);
	return jadwal_imunisasi;
};
