'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class jenis_imunisasi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  jenis_imunisasi.init({
    jenis_imunisasi: DataTypes.STRING,
    umur: DataTypes.STRING,
    satuan_umur: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'jenis_imunisasi',
  });
  return jenis_imunisasi;
};