"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class auth_device extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			auth_device.belongsTo(models.profile, {
				foreignKey: "id_auth",
				targetKey: "id_auth",
				sourceKey: "id_auth",
				as: "device",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	auth_device.init(
		{
			id_auth: DataTypes.STRING,
			imei_device: DataTypes.STRING,
			hardware: DataTypes.STRING,
			manufacturer: DataTypes.STRING,
			type: DataTypes.STRING,
			os_name: DataTypes.STRING,
			status: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "auth_device",
		}
	);
	return auth_device;
};
