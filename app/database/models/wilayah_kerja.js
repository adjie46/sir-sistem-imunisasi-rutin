"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class wilayah_kerja extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	wilayah_kerja.init(
		{
			id_puskesmas: DataTypes.STRING,
			nama_posyandu: DataTypes.STRING,
			nama_desa: DataTypes.STRING,
			status: DataTypes.BOOLEAN,
		},
		{
			sequelize,
			modelName: "wilayah_kerja",
		}
	);
	return wilayah_kerja;
};
