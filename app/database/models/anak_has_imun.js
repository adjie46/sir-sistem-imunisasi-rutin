"use strict";
const { Model } = require("sequelize");
const moment = require("moment");
module.exports = (sequelize, DataTypes) => {
	class anak_has_imun extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			anak_has_imun.hasMany(models.anggota_keluarga, {
				foreignKey: "id",
				targetKey: "id",
				sourceKey: "id_anak",
				as: "anggota_keluarga",
			});
			anak_has_imun.hasOne(models.jenis_imunisasi, {
				foreignKey: "id",
				targetKey: "id",
				sourceKey: "id_imun",
				as: "jenis_imunisasi",
			});
		}
	}
	anak_has_imun.init(
		{
			id_anak: DataTypes.STRING,
			id_imun: DataTypes.STRING,
			tgl_imun: DataTypes.DATE,
		},
		{
			sequelize,
			modelName: "anak_has_imun",
			getterMethods: {
				tgl_imunisasi_format() {
					var date;
					date = moment(this.getDataValue("tgl_imun")).format("DD-MM-YYYY");
					if (date == "Invalid date") {
						date = null;
					}
					return date;
				},
			},
		}
	);
	return anak_has_imun;
};
