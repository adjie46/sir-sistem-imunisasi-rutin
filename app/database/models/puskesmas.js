"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class puskesmas extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	puskesmas.init(
		{
			id_kecamatan: DataTypes.STRING,
			nama_puskesmas: DataTypes.STRING,
			status: DataTypes.BOOLEAN,
		},
		{
			sequelize,
			modelName: "puskesmas",
		}
	);
	return puskesmas;
};
