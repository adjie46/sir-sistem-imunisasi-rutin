'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class app_setting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  app_setting.init({
    app_name: DataTypes.STRING,
    app_package: DataTypes.STRING,
    app_type: DataTypes.STRING,
    app_token: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'app_setting',
  });
  return app_setting;
};