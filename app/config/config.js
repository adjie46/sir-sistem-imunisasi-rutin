require("dotenv").config();

module.exports = {
	baseUrl: process.env.BASE_URL,
	hex: process.env.HEX,
	port: process.env.PORT,
	jwtSecret: process.env.JWT_SECRET,
	jwtExpireWeb: process.env.JWT_EXPIRE,
	jwtExpireMobile: process.env.JWT_EXPIRE_MOBILE,
	jwtExpireReset: process.env.JWT_EXPIRE_RESET,
	mode: process.env.MODE,
	whitelist: process.env.WHITELIST,
	limitBody: process.env.LIMIT_BODY,
	nameSpace: process.env.MY_NAMESPACE,
	userAgent: process.env.USER_AGENT,
	midtrans_serverkey: process.env.serverKey,
	midtrans_clientKey: process.env.clientKey,
	time_send: process.env.TIME,

	//TTE
	apiSignDev: process.env.URL_SIGN_DEV,
	apiSignProd: process.env.URL_SIGN_PROD,
	baseUrlApiSignProd: process.env.BASE_URL_SIGN_PROD,
	baseUrlApiSignDev: process.env.BASE_URL_SIGN_DEV,
	usernameSign_dev: process.env.SIGN_USERNAME_DEV,
	passwordSign_dev: process.env.SIGN_PASSWORD_DEV,
	usernameSign_prod: process.env.SIGN_USERNAME_PROD,
	passwordSign_prod: process.env.SIGN_PASSWORD_PROD,
	urlAnchor: process.env.URL_ANCHOR,

	tampilan: process.env.TAMPILAN,
	width: process.env.WIDTH,
	height: process.env.HEIGHT,
	headers: process.env.USER_AGENT_API,

	//MOBILE
	forceUpdate: process.env.FORCE_UPDATE,
	versiApps: process.env.VERSI_APPS,

	//WA
	instanceId: process.env.INSTANCE_ID,
	accessToken: process.env.ACCESS_TOKEN,
	message: process.env.PESAN,
};
