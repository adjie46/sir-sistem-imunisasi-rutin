$(document).ready(function () {
	var m = $("meta[name=X-ID-Compatible]");
	var n = $("meta[name=X-ID-Indonesia]");

	table = $("#tbl_app_settings").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			type: "GET",
			url: "/setting/android",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data API KEY Tidak Tersedia",
			infoEmpty: "Data API KEY Tidak Tersedia",
		},
		columns: [null, { visible: false }, null, null, null, null],
		columnDefs: [
			{
				render: function (data) {
					return `
						<td>
                            ${data[1]}
                        </td>
					`;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
						<td>
                            ${data[2]}
                        </td>
					`;
				},
				data: null,
				targets: [1],
			},
			{
				render: function (data) {
					return `
						<td>
                            ${data[3]}
                        </td>
					`;
				},
				data: null,
				targets: [2],
			},
			{
				render: function (data) {
					if (data[4] == "Android") {
						return `
						<td>
                            <span class="mb-1 badge bg-success">${data[4]}</span>
                        </td>
					`;
					} else if (data[4] == "Website") {
						return `
						<td>
                            <span class="mb-1 badge bg-warning">${data[4]}</span>
                        </td>
					`;
					} else {
						return `
						<td>
                            <span class="mb-1 badge bg-primary">${data[4]}</span>
                        </td>
					`;
					}
				},
				data: null,
				targets: [3],
			},
			{
				render: function (data) {
					return `
						<td>
                            <b>${data[5]}</b>
                        </td>
					`;
				},
				data: null,
				targets: [4],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="copy_api_key" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Copy API KEY" class="text-dark block ms-2">
                                    <i class="ti ti-copy fs-5"></i>
                                </id=>
                                <a href="javascript:void(0)" id="del_api_key" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus API KEY" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});

	$('button[data-bs-toggle="pill"]').on("shown.bs.tab", function (e) {
		if (e.target.innerText == "Android") {
			table.columns.adjust().draw();
		}
	});

	$(document).on("click", "#del_api_key", function () {
		var id = $(this).attr("data-id");
		Swal.fire({
			title: "Yakin ingin menghapus data ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, Hapus!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: () => {
				return fetch(`/setting/android/${id}`, {
					method: "delete",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error.message}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(result) => {
						location.reload();
					}
				);
			} else {
				Swal.fire("Gagal!", result.value.message, "error").then((result) => {
					location.reload();
				});
			}
		});
	});

	$(document).on("click", "#copy_api_key", function () {
		var id = $(this).attr("data-id");
		Swal.fire({
			title: "Yakin ingin mengcopy API KEY ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, Copy!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: () => {
				return fetch(`/setting/android/${id}`, {
					method: "put",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error.message}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(results) => {
						navigator.clipboard.writeText(result.value.data.app_token);
					}
				);
			} else {
				Swal.fire("Gagal!", result.value.message, "error").then((result) => {
					location.reload();
				});
			}
		});
	});

	$("#frmAddApiKey").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();

			var values = $(this).serializeArray();

			$.ajax({
				type: "post",
				headers: {
					"X-CSRF-Token": m.attr("content"),
				},
				url: "/setting/android",
				data: values,
				dataType: "json",
				beforeSend: function () {
					$(".modal-content").LoadingOverlay("show", {
						background: "rgba(165, 190, 100, 0.5)",
					});
				},
				success: function (response) {
					if (response.success) {
						$(".modal-content").LoadingOverlay("hide", true);
						$("#addAPI").modal("toggle");
						Swal.fire({
							type: "success",
							title: "<b>Sukses!</b>",
							html: response.message,
							confirmButtonText: "OK",
						}).then((result) => {
							location.reload();
						});
					} else {
						$("#addAPI").modal("toggle");
						$(".modal-content").LoadingOverlay("hide", true);
						Swal.fire({
							type: "error",
							title: "<b>Gagal!</b>",
							html: response.message,
							confirmButtonText: "OK",
						}).then((result) => {
							location.reload();
						});
					}
				},
				error: function (xhr, status, errorThrown) {
					if (xhr.statusText == "Unauthorized") {
						location.replace("/");
					} else {
						$(".modal-content").LoadingOverlay("hide", true);
						$("#addAPI").modal("toggle");
						Swal.fire({
							type: "error",
							title: "<b>Gagal!</b>",
							html: xhr.responseJSON.message,
							confirmButtonText: "OK",
						}).then((result) => {
							location.reload();
						});
					}
				},
			});
		}
		$(this).addClass("was-validated");
	});
});
