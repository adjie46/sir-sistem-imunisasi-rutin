$(function () {
	var m = $("meta[name=X-ID-Compatible]");
	var n = $("meta[name=X-ID-Indonesia]");

	var currentURL = window.location.href.split("=");

	$("#addJadwal").on("click", function (event) {
		$("#addJadwal #btn-add").show();
		$("#addJadwal #btn-edit").hide();
		$("#_actions").val("add");
		$(".title_opd").html("Tambah Jadwal Imunisasi");
	});

	$("#kecamatan").on("change", function (e) {
		var id = this.value;

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/puskesmas/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				$("#select_puskesmas").empty();

				if (response.success) {
					$(".modal-content").LoadingOverlay("hide", true);

					if (response.data.length > 0) {
						$.each(response.data, function (index, item) {
							$("#select_puskesmas").append(
								new Option(item.nama_puskesmas, item.id, false, false)
							);
						});
						$("#addKeluarga #lyt_puskesmas").show();
					} else {
						$("#addKeluarga #lyt_puskesmas").hide();
						Swal.fire({
							type: "error",
							title: "<b>Yaaahh!</b>",
							html: "Data Puskesmas Belum Ada",
							confirmButtonText: "OK",
						});
					}
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			error: function (xhr, status, errorThrown) {
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#addKeluarga").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
		});
	});

	$("#status_keluarga").on("change", function (e) {
		var id = this.value;
		if (id == 2) {
			$("#addAnggotaKeluarga #lyt_status_keluarga").removeClass("col-md-6");
			$("#addAnggotaKeluarga #lyt_status_keluarga").addClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").removeClass("col-md-6");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").addClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_anak_ke").show();
		} else {
			$("#addAnggotaKeluarga #lyt_anak_ke").hide();
			$("#addAnggotaKeluarga #lyt_status_keluarga").removeClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_status_keluarga").addClass("col-md-6");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").removeClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").addClass("col-md-6");
		}
	});

	$("#select_puskesmas").on("change", function (e) {
		var id = this.value;

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/wilayah_kerja/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				$("#select_wilayah_kerja").empty();

				if (response.success) {
					$(".modal-content").LoadingOverlay("hide", true);

					if (response.data.length > 0) {
						$.each(response.data, function (index, item) {
							$("#select_wilayah_kerja").append(
								new Option(item.nama_posyandu, item.id, false, false)
							);
						});
						$("#addKeluarga #lyt_w_kerja").show();
					} else {
						$("#addKeluarga #lyt_w_kerja").hide();
						Swal.fire({
							type: "error",
							title: "<b>Yaaahh!</b>",
							html: "Data Wilayah Kerja Belum Ada",
							confirmButtonText: "OK",
						});
					}
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			error: function (xhr, status, errorThrown) {
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#addKeluarga").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
		});
	});

	$("#frmAddJadwal").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var values = $(this).serializeArray();
			var actions = $("#_actions").val();

			id = $("#_id").val();

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/jadwal_imunisasi",
					data: values,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addJadwal").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addJadwal").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addJadwal").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});

	var jadwal_imunisasi = $("#tbl_jadwal_imunisasi").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			headers: {
				id_puskesmas: currentURL[2],
			},
			type: "GET",
			url: "/jadwal_imunisasi",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Jadwal Imunisasi Tidak Tersedia",
			infoEmpty: "Data Jadwal Imunisasi Tidak Tersedia",
		},
		columns: [null, null, null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = jadwal_imunisasi.page.info().page;
					var pageLength = jadwal_imunisasi.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
						<td>
            				<p style="color:black;font-weight: bold;">${data[1]}</p>
        				</td>
					`;
				},
				data: null,
				targets: [1],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="edit_kk" data-bs-toggle="modal" data-bs-target="#addKeluarga" data-id='${data[0]}'
									data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Edit Kartu Keluarga" class="text-dark edit ms-2">
                                 	<i class="ti ti-edit fs-5"></i>
                                </a>
                                <a href="javascript:void(0)" id="hapus_kk" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Kartu Keluarga" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>  
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});
});
