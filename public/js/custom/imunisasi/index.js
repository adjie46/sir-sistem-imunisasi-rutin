$(function () {
	var m = $("meta[name=X-ID-Compatible]");
	var n = $("meta[name=X-ID-Indonesia]");

	var currentURL = window.location.href.split("=");

	$(".select2").select2({
		width: "90%",
		placeholder: "Silahkan Pilih Instansi",
		theme: "classic",
		dropdownParent: $("#addImunisasi"),
	});

	$("#btn_add_jenis_imun").on("click", function (event) {
		$("#addJenisImunisasi #btn-add").show();
		$("#addJenisImunisasi #btn-edit").hide();
		$("#_actions").val("add");
		$(".title_opd").html("Tambah Jenis Imuniasasi");
	});

	$("#btn_add_imunisasi").on("click", function (event) {
		$("#addImunisasi #btn-add").show();
		$("#addImunisasi #btn-edit").hide();
		$("#_actions").val("add");
		$(".title_opd").html("Tambah Data Imunisasi");
	});

	var dataTable = $("#tbl_jenis_imunisasi").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			type: "GET",
			url: "/jenis_imunisasi",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Jenis Imunisasi Tidak Tersedia",
			infoEmpty: "Data Jenis Imunisasi Tidak Tersedia",
		},
		columns: [null, null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = dataTable.page.info().page;
					var pageLength = dataTable.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="hapus_jenis_imunisasi" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Jenis Imunisasi" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});

	var dataTable = $("#tbl_data_imunisasi").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			headers: {
				id_anak: currentURL[2],
			},
			type: "GET",
			url: "/data_imunisasi",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Jenis Imunisasi Tidak Tersedia",
			infoEmpty: "Data Jenis Imunisasi Tidak Tersedia",
		},
		columns: [null, null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = dataTable.page.info().page;
					var pageLength = dataTable.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="hapus_jenis_imunisasi" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Jenis Imunisasi" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});

	$("#frmAddJenisImun").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var values = $(this).serializeArray();
			var actions = $("#_actions").val();

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/jenis_imunisasi",
					data: values,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addJenisImunisasi").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addJenisImunisasi").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addJenisImunisasi").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});

	$("#frmAddImun").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var values = $(this).serializeArray();
			var actions = $("#_actions").val();

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/data_imunisasi",
					data: values,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addImunisasi").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addImunisasi").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addImunisasi").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});
});
