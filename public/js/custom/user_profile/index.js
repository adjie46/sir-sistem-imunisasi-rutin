$(document).ready(function () {
	var m = $("meta[name=X-ID-Compatible]");
	var n = $("meta[name=X-ID-Indonesia]");

	function loadButton(idButton, type) {
		if (type == 1) {
			$(idButton).prop("disabled", true);
			$(idButton).html(
				`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading`
			);
		} else if (type == 0) {
			$(idButton).prop("disabled", false);
			$(idButton).html(`Sign In`);
		} else if (type == 2) {
			$(idButton).prop("disabled", true);
			$(idButton).html(`Sign In`);
		}
	}

	function notification(id, type, message) {
		$(id).html(`
			<div class="alert customize-alert alert-dismissible text-${type} border border-${type} fade show remove-close-icon" role="alert">
            	<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                	<div class="d-flex align-items-center font-medium me-3 me-md-0">
                        <i class="ti ti-info-circle fs-5 me-2 flex-shrink-0 text-${type}"></i>
                        ${message}
                    </div>
            </div>`);
	}

	$("#show_hide_password_profile a").on("click", function (event) {
		event.preventDefault();
		if ($("#old_password").attr("type") == "text") {
			$("#old_password").attr("type", "password");
			$("#iconPass").removeClass("ti-eye");
			$("#iconPass").addClass("ti-eye-closed");
		} else if ($("#old_password").attr("type") == "password") {
			$("#old_password").attr("type", "text");
			$("#iconPass").removeClass("ti-eye-closed");
			$("#iconPass").addClass("ti-eye");
		}
	});

	$("#show_hide_password_profile_new a").on("click", function (event) {
		event.preventDefault();
		if ($("#new_password").attr("type") == "text") {
			$("#new_password").attr("type", "password");
			$("#iconPass_new").removeClass("ti-eye");
			$("#iconPass_new").addClass("ti-eye-closed");
		} else if ($("#new_password").attr("type") == "password") {
			$("#new_password").attr("type", "text");
			$("#iconPass_new").removeClass("ti-eye-closed");
			$("#iconPass_new").addClass("ti-eye");
		}
	});

	$("#show_hide_password_profile_konfirm a").on("click", function (event) {
		event.preventDefault();
		if ($("#konfirm_password").attr("type") == "text") {
			$("#konfirm_password").attr("type", "password");
			$("#iconPass_konfirm").removeClass("ti-eye");
			$("#iconPass_konfirm").addClass("ti-eye-closed");
		} else if ($("#konfirm_password").attr("type") == "password") {
			$("#konfirm_password").attr("type", "text");
			$("#iconPass_konfirm").removeClass("ti-eye-closed");
			$("#iconPass_konfirm").addClass("ti-eye");
		}
	});

	$("#frm_change_password").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();

			var formData = new FormData();
			params = $(this).serializeArray();

			$.each(params, function (i, val) {
				formData.append(val.name, val.value);
			});

			id = $("#_id").val();

			$.ajax({
				type: "post",
				headers: {
					"X-CSRF-Token": $("input[name=_csrf]").val(),
				},
				url: `/user/${id}/change_password`,
				data: formData,
				dataType: "json",
				contentType: false,
				processData: false,
				beforeSend: function () {
					loadButton("#change_password_btn", 1);
				},
				success: function (response) {
					if (response.success) {
						loadButton("#change_password_btn", 2);
						notification("#notification", "success", response.message);
						setTimeout(() => {
							location.reload();
						}, 2000);
					} else {
						setTimeout(() => {
							loadButton("#change_password_btn", 0);
							notification("#notification", "danger", response.message);

							setTimeout(() => {
								$(".alert")
									.delay(4000)
									.slideUp(200, function () {
										$(this).alert("close");
									});
							}, 500);
						}, 1000);
					}
				},
				error: function (xhr, status, errorThrown) {
					if (xhr.statusText == "Unauthorized") {
						location.replace("/");
					} else if (xhr.responseJSON.status == 2) {
						location.replace("/maintenance");
					} else {
						setTimeout(() => {
							loadButton("#btn_loading", 0);
							notification("#notification", "danger", xhr.responseJSON.message);

							setTimeout(() => {
								$(".alert")
									.delay(4000)
									.slideUp(200, function () {
										$(this).alert("close");
									});
							}, 500);
						}, 1000);
					}
				},
			});
		}
		$(this).addClass("was-validated");
	});

	$("#notif_android").on("click", function (e) {
		const isChecked = $(this).is(":checked");
		id = $("#_id").val();
		if (isChecked) {
			aktif = "mengaktifkan";
		} else {
			aktif = "menonaktifkan";
		}
		Swal.fire({
			title: `Yakin ingin ${aktif}  notifikasi?`,
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: `Ya, ${aktif}!`,
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: async () => {
				return fetch(`/user/${id}/notif_android_${isChecked}`, {
					method: "put",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (jQuery.isEmptyObject(result.value)) {
				if (isChecked) {
					$("#notif_android").prop("checked", false);
					console.log("HARUSNYA FALSE");
				} else {
					$("#notif_android").prop("checked", true);
					console.log("HARUSNYA TRUE");
				}
			} else {
				console.log("ENGGA");
				if (result.value.success) {
					Swal.fire("Berhasil!", result.value.message, "success").then(
						(result) => {
							location.reload();
						}
					);
				} else {
					Swal.fire("Gagal!", result.value.message, "error").then((result) => {
						location.reload();
					});
				}
			}
		});
	});

	$("#notif_wa").on("click", function (e) {
		const isChecked = $(this).is(":checked");
		id = $("#_id").val();
		if (isChecked) {
			aktif = "mengaktifkan";
		} else {
			aktif = "menonaktifkan";
		}
		Swal.fire({
			title: `Yakin ingin ${aktif}  notifikasi?`,
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: `Ya, ${aktif}!`,
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: async () => {
				return fetch(`/user/${id}/notif_wa_${isChecked}`, {
					method: "put",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (jQuery.isEmptyObject(result.value)) {
				if (isChecked) {
					$("#notif_wa").prop("checked", false);
					console.log("HARUSNYA FALSE");
				} else {
					$("#notif_wa").prop("checked", true);
					console.log("HARUSNYA TRUE");
				}
			} else {
				console.log("ENGGA");
				if (result.value.success) {
					Swal.fire("Berhasil!", result.value.message, "success").then(
						(result) => {
							location.reload();
						}
					);
				} else {
					Swal.fire("Gagal!", result.value.message, "error").then((result) => {
						location.reload();
					});
				}
			}
		});
	});
});
