$(function () {
	var m = $("meta[name=X-ID-Compatible]");
	var n = $("meta[name=X-ID-Indonesia]");

	var currentURL = window.location.href.split("=");

	$("#btn_add_kecamatan").on("click", function (event) {
		$("#addKecamatan #btn-add").show();
		$("#addKecamatan #btn-edit").hide();
		$("#_actions").val("add");
		$(".title_opd").html("Tambah Kecamatan");
	});

	$("#btn_add_puskesmas").on("click", function (event) {
		$("#addPuskesmas #btn-add").show();
		$("#addPuskesmas #btn-edit").hide();
		$("#_actions").val("add");
		$(".title_opd").html("Tambah Puskesmas");
	});

	$("#btn_add_w_kerja").on("click", function (event) {
		$("#addWKerja #btn-add").show();
		$("#addWKerja #btn-edit").hide();
		$("#_actions").val("add");
		$(".title_opd").html("Tambah Wilayah Kerja");
	});

	$("#btn_add_keluarga").on("click", function (event) {
		$("#addKeluarga #btn-add").show();
		$("#addKeluarga #btn-edit").hide();
		$("#addKeluarga #lyt_w_kerja").hide();
		$("#addKeluarga #lyt_puskesmas").hide();
		$("#_actions").val("add");
		$(".title_opd").html("Tambah Kartu Keluarga");
	});

	$("#btn_add_anggota_keluarga").on("click", function (event) {
		$("#addAnggotaKeluarga #btn-add").show();
		$("#addAnggotaKeluarga #btn-edit").hide();
		$("#addAnggotaKeluarga #lyt_anak_ke").hide();
		$("#addAnggotaKeluarga #lyt_status_keluarga").removeClass("col-md-4");
		$("#addAnggotaKeluarga #lyt_status_keluarga").addClass("col-md-6");
		$("#addAnggotaKeluarga #lyt_kepala_keluarga").removeClass("col-md-4");
		$("#addAnggotaKeluarga #lyt_kepala_keluarga").addClass("col-md-6");

		$("#_actions").val("add");
		$(".title_opd").html("Tambah Anggota Keluarga");
	});

	$("#kecamatan").on("change", function (e) {
		var id = this.value;

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/puskesmas/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				$("#select_puskesmas").empty();

				if (response.success) {
					$(".modal-content").LoadingOverlay("hide", true);

					if (response.data.length > 0) {
						$.each(response.data, function (index, item) {
							$("#select_puskesmas").append(
								new Option(item.nama_puskesmas, item.id, false, false)
							);
						});
						$("#addKeluarga #lyt_puskesmas").show();
					} else {
						$("#addKeluarga #lyt_puskesmas").hide();
						Swal.fire({
							type: "error",
							title: "<b>Yaaahh!</b>",
							html: "Data Puskesmas Belum Ada",
							confirmButtonText: "OK",
						});
					}
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			error: function (xhr, status, errorThrown) {
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#addKeluarga").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
		});
	});

	$("#status_keluarga").on("change", function (e) {
		var id = this.value;
		if (id == 2) {
			$("#addAnggotaKeluarga #lyt_status_keluarga").removeClass("col-md-6");
			$("#addAnggotaKeluarga #lyt_status_keluarga").addClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").removeClass("col-md-6");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").addClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_anak_ke").show();
		} else {
			$("#addAnggotaKeluarga #lyt_anak_ke").hide();
			$("#addAnggotaKeluarga #lyt_status_keluarga").removeClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_status_keluarga").addClass("col-md-6");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").removeClass("col-md-4");
			$("#addAnggotaKeluarga #lyt_kepala_keluarga").addClass("col-md-6");
		}
	});

	$("#select_puskesmas").on("change", function (e) {
		var id = this.value;

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/wilayah_kerja/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				$("#select_wilayah_kerja").empty();

				if (response.success) {
					$(".modal-content").LoadingOverlay("hide", true);

					if (response.data.length > 0) {
						$.each(response.data, function (index, item) {
							$("#select_wilayah_kerja").append(
								new Option(item.nama_posyandu, item.id, false, false)
							);
						});
						$("#addKeluarga #lyt_w_kerja").show();
					} else {
						$("#addKeluarga #lyt_w_kerja").hide();
						Swal.fire({
							type: "error",
							title: "<b>Yaaahh!</b>",
							html: "Data Wilayah Kerja Belum Ada",
							confirmButtonText: "OK",
						});
					}
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			error: function (xhr, status, errorThrown) {
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#addKeluarga").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
		});
	});

	var dataTable = $("#tbl_kecamatan").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			type: "GET",
			url: "/kecamatan",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Kecamatan Tidak Tersedia",
			infoEmpty: "Data Kecamatan Tidak Tersedia",
		},
		columns: [null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = dataTable.page.info().page;
					var pageLength = dataTable.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
						<td>
            				<a style="color:black" href="?pages=detail_kecamatan&id=${data[0]}">${data[1]}</a>
        				</td>
					`;
				},
				data: null,
				targets: [1],
			},
			{
				render: function (data) {
					if (data[2] == "1") {
						return `
						<td>
							<span class="badge rounded-pill bg-success">Aktif</span>
						</td>
						`;
					} else if (data[2] == 0) {
						return `
						<td>
							<span class="badge rounded-pill bg-danger">Nonaktif</span>
						</td>
						`;
					}
				},
				data: null,
				targets: [2],
			},
			{
				render: function (data) {
					if (data[2] == "1") {
						return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="disabled_kecamatan" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Nonaktifkan Kecamatan" class="text-dark block ms-2">
                                    <i class="ti ti-square-rounded-x fs-5"></i>
                                </id=>
                                <a href="javascript:void(0)" id="edit_kecamatan" data-id='${data[0]}'
									data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Edit Nama Kecamatan" class="text-dark edit ms-2">
                                 	<i class="ti ti-edit fs-5"></i>
                                </a>
                                <a href="javascript:void(0)" id="hapus_kecamatan" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Kecamatan" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>
						`;
					} else {
						return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="enabled_kecamatan" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Nonaktifkan Kecamatan" class="text-dark block ms-2">
                                    <i class="ti ti-checks fs-5"></i>
                                </a>
                                <a href="javascript:void(0)" id="edit_kecamatan" data-bs-toggle="modal" data-bs-target="#editOpd" data-id='${data[0]}'
									data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Edit Nama Kecamatan" class="text-dark edit ms-2">
                                 	<i class="ti ti-edit fs-5"></i>
                                </a>
                                <a href="javascript:void(0)" id="hapus_kecamatan" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Kecamatan" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>  
						`;
					}
				},
				data: null,
				targets: [-1],
			},
		],
	});

	var Puskesmas = $("#tbl_puskesmas").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			headers: {
				id_kecamatan: currentURL[2],
			},
			type: "GET",
			url: "/puskesmas",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Puskesmas Tidak Tersedia",
			infoEmpty: "Data Puskesmas Tidak Tersedia",
		},
		columns: [null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = Puskesmas.page.info().page;
					var pageLength = Puskesmas.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
						<td>
            				<a style="color:black" href="?pages=detail_puskesmas&id=${data[0]}">${data[1]}</a>
        				</td>
					`;
				},
				data: null,
				targets: [1],
			},
			{
				render: function (data) {
					if (data[2] == "true") {
						return `
						<td>
							<span class="badge rounded-pill bg-success">Aktif</span>
						</td>
						`;
					} else if (data[2] == "false") {
						return `
						<td>
							<span class="badge rounded-pill bg-danger">Nonaktif</span>
						</td>
						`;
					} else {
						return `
						<td>
							<span class="badge rounded-pill bg-danger">Error</span>
						</td>
						`;
					}
				},
				data: null,
				targets: [2],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="hapus_puskesmas" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Kecamatan" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});

	var wilayah_kerja = $("#tbl_wilayah_kerja").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			headers: {
				id_puskesmas: currentURL[2],
			},
			type: "GET",
			url: "/wilayah_kerja",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Wilayah Kerja Tidak Tersedia",
			infoEmpty: "Data Wilayah Kerja Tidak Tersedia",
		},
		columns: [null, null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = wilayah_kerja.page.info().page;
					var pageLength = wilayah_kerja.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
						<td>
            				<a style="color:black" href="#">${data[1]}</a>
        				</td>
					`;
				},
				data: null,
				targets: [1],
			},
			{
				render: function (data) {
					if (data[3] == "true") {
						return `
						<td>
							<span class="badge rounded-pill bg-success">Aktif</span>
						</td>
						`;
					} else if (data[3] == "false") {
						return `
						<td>
							<span class="badge rounded-pill bg-danger">Nonaktif</span>
						</td>
						`;
					} else {
						return `
						<td>
							<span class="badge rounded-pill bg-danger">Error</span>
						</td>
						`;
					}
				},
				data: null,
				targets: [3],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="edit_w_kerja" data-bs-toggle="modal" data-bs-target="#editOpd" data-id='${data[0]}'
									data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Edit Nama Kecamatan" class="text-dark edit ms-2">
                                 	<i class="ti ti-edit fs-5"></i>
                                </a>
                                <a href="javascript:void(0)" id="hapus_w_kerja" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Kecamatan" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>  
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});

	var kartu_keluarga = $("#tbl_keluarga").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			headers: {
				id_puskesmas: currentURL[2],
			},
			type: "GET",
			url: "/kartu_keluarga",
			dataSrc: function (json) {
				return json.data;
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Kartu Keluarga Tidak Tersedia",
			infoEmpty: "Data Kartu Keluarga Tidak Tersedia",
		},
		columns: [null, null, null, null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = kartu_keluarga.page.info().page;
					var pageLength = kartu_keluarga.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					return `
						<td>
            				<a style="color:black;font-weight: bold;" href="?pages=detail_kk&id=${data[0]}">${data[1]}</a>
        				</td>
					`;
				},
				data: null,
				targets: [1],
			},
			{
				render: function (data) {
					if (data[2] != "") {
						return `
						<td>
            				${data[2]}
        				</td>
						`;
					} else {
						return `<span class="mb-1 badge bg-danger">Tidak Ada Data</span>`;
					}
				},
				data: null,
				targets: [2],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="edit_kk" data-bs-toggle="modal" data-bs-target="#addKeluarga" data-id='${data[0]}'
									data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Edit Kartu Keluarga" class="text-dark edit ms-2">
                                 	<i class="ti ti-edit fs-5"></i>
                                </a>
                                <a href="javascript:void(0)" id="hapus_kk" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Kartu Keluarga" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>  
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});

	var anggota_keluarga = $("#tbl_anggota_keluarga").DataTable({
		processing: true,
		serverSide: true,
		paging: true,
		responsive: true,
		searching: true,
		ordering: false,
		scrollX: true,
		scrollY: true,
		ajax: {
			headers: {
				id_kk: currentURL[2],
			},
			type: "GET",
			url: "/anggota_keluarga",
			dataSrc: function (json) {
				console.log(json.data);
				return json.data;
			},
			error: function (xhr, errorType, exception) {
				console.log(xhr.responseText);
			},
		},
		stateSave: true,
		language: {
			emptyTable: "Data Anggota Keluarga Tidak Tersedia",
			infoEmpty: "Data Anggota Keluarga Tidak Tersedia",
		},
		columns: [null, null, null, null, null, null, null, null, null, null],
		columnDefs: [
			{
				render: function (data, type, row, meta) {
					var currentPage = anggota_keluarga.page.info().page;
					var pageLength = anggota_keluarga.page.info().length; // Get the page length
					var rowNumber = meta.row + 1; // Get the row index

					// Calculate the auto number for the current row
					var autoNumber = currentPage * pageLength + rowNumber;

					return autoNumber;
				},
				data: null,
				targets: [0],
			},
			{
				render: function (data) {
					if (data[3] == "true") {
						return `<span class="mb-1 badge bg-secondary">Ya</span>`;
					} else {
						return `<span class="mb-1 badge bg-danger">Tidak</span>`;
					}
				},
				data: null,
				targets: [3],
			},
			{
				render: function (data) {
					if (data[4] == "") {
						return `-`;
					} else {
						return `${data[4]}`;
					}
				},
				data: null,
				targets: [4],
			},
			{
				render: function (data) {
					if (data[5] == "true") {
						return `
						<td>
            				<a style="color:black;font-weight: bold;" href="?pages=imunisasi&id=${data[0]}">${data[1]}</a>
        				</td>`;
					} else {
						return `${data[1]}`;
					}
				},
				data: null,
				targets: [1],
			},
			{
				render: function (data) {
					if (data[3] == "true") {
						return `<span class="mb-1 badge bg-info">Ayah</span>`;
					} else if (data[5] == "true") {
						return `<span class="mb-1 badge bg-success">Anak</span>`;
					} else if (data[3] == "false") {
						return `<span class="mb-1 badge bg-secondary">Ibu</span>`;
					}
				},
				data: null,
				targets: [5],
			},
			{
				render: function (data) {
					return `
							<div class="action-btn">
                                <a href="javascript:void(0)" id="edit_anggota_keluarga" data-bs-toggle="modal" data-bs-target="#addAnggotaKeluarga" data-id='${data[0]}'
									data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Edit Anggota Keluarga" class="text-dark edit ms-2">
                                 	<i class="ti ti-edit fs-5"></i>
                                </a>
                                <a href="javascript:void(0)" id="hapus_anggota_keluarga" data-id='${data[0]}' data-bs-toggle="tooltip" data-bs-placement="bottom"
                                	title="Hapus Anggota Keluarga" class="text-dark delete ms-2">
                                    <i class="ti ti-trash fs-5"></i>
                                </a>
                            </div>  
						`;
				},
				data: null,
				targets: [-1],
			},
		],
	});

	$(document).on("click", "#edit_kecamatan", function (e) {
		$("#addKecamatan #btn-add").hide();
		$("#addKecamatan #btn-edit").show();
		$(".title_opd").html("Edit Kecamatan");
		$("#addKecamatan").modal("show");

		$("#_actions").val("update");

		var id = $(this).attr("data-id");

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/kecamatan/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				if (response.success) {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#_id").val(response.data.id);
					$("input[name='nama_kecamatan']").val(response.data.nama_kecamatan);
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			error: function (xhr, status, errorThrown) {
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#addKecamatan").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
		});
	});

	$(document).on("click", "#hapus_kecamatan", function () {
		var id = $(this).attr("data-id");
		Swal.fire({
			title: "Yakin ingin menghapus Kecamatan ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, Hapus!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: () => {
				return fetch(`/kecamatan/${id}`, {
					method: "delete",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error.message}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(result) => {
						location.reload();
					}
				);
			} else {
				Swal.fire("Gagal!", result.value.message, "error").then((result) => {
					location.reload();
				});
			}
		});
	});

	$(document).on("click", "#hapus_puskesmas", function () {
		var id = $(this).attr("data-id");
		Swal.fire({
			title: "Yakin ingin menghapus Puskesmas ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, Hapus!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: () => {
				return fetch(`/puskesmas/${id}`, {
					method: "delete",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error.message}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(result) => {
						location.reload();
					}
				);
			} else {
				Swal.fire("Gagal!", result.value.message, "error").then((result) => {
					location.reload();
				});
			}
		});
	});

	$(document).on("click", "#hapus_w_kerja", function () {
		var id = $(this).attr("data-id");
		Swal.fire({
			title: "Yakin ingin menghapus Wilayah Kerja ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, Hapus!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: () => {
				return fetch(`/wilayah_kerja/${id}`, {
					method: "delete",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error.message}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(result) => {
						location.reload();
					}
				);
			} else {
				Swal.fire("Gagal!", result.value.message, "error").then((result) => {
					location.reload();
				});
			}
		});
	});

	$("#frmAddOpd").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var values = $(this).serializeArray();
			var actions = $("#_actions").val();

			id = $("#_id").val();

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/kecamatan",
					data: values,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addKecamatan").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			} else if (actions == "update") {
				$.ajax({
					type: "put",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: `/kecamatan/${id}`,
					data: values,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addKecamatan").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});

	$("#frmAddPuskesmas").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var formData = $(this).serializeArray();
			var actions = $("#_actions").val();

			var currentURL = window.location.href.split("=");

			var additionalData = {
				id_kecamatan: currentURL[2],
			};

			var mergedData = formData.concat(
				Object.entries(additionalData).map(([name, value]) => ({ name, value }))
			);

			id = $("#_id").val();

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/puskesmas",
					data: mergedData,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addPuskesmas").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addPuskesmas").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addPuskesmas").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			} else if (actions == "update") {
				$.ajax({
					type: "put",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: `/kecamatan/${id}`,
					data: values,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addKecamatan").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});

	$("#frmAddWKerja").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var formData = $(this).serializeArray();
			var actions = $("#_actions").val();

			var currentURL = window.location.href.split("=");

			var additionalData = {
				id_puskesmas: currentURL[2],
			};

			var mergedData = formData.concat(
				Object.entries(additionalData).map(([name, value]) => ({ name, value }))
			);

			id = $("#_id").val();

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/wilayah_kerja",
					data: mergedData,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addWKerja").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addWKerja").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addWKerja").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			} else if (actions == "update") {
				$.ajax({
					type: "put",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: `/kecamatan/${id}`,
					data: values,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addKecamatan").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKecamatan").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});

	$("#frmAddKeluarga").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var formData = $(this).serializeArray();
			var actions = $("#_actions").val();

			id = $("#_id").val();

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/kartu_keluarga",
					data: formData,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKeluarga").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addKeluarga").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addKeluarga").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});

	$("#frmAddAnggotaKeluarga").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();
			var formData = $(this).serializeArray();
			var actions = $("#_actions").val();

			var currentURL = window.location.href.split("=");

			var additionalData = {
				id_kk: currentURL[2],
			};

			var mergedData = formData.concat(
				Object.entries(additionalData).map(([name, value]) => ({ name, value }))
			);

			if (actions == "add") {
				$.ajax({
					type: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
					url: "/anggota_keluarga",
					data: mergedData,
					dataType: "json",
					beforeSend: function () {
						$(".modal-content").LoadingOverlay("show", {
							background: "rgba(165, 190, 100, 0.5)",
						});
					},
					success: function (response) {
						if (response.success) {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addAnggotaKeluarga").modal("toggle");
							Swal.fire({
								type: "success",
								title: "<b>Sukses!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						} else {
							$("#addAnggotaKeluarga").modal("toggle");
							$(".modal-content").LoadingOverlay("hide", true);
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: response.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
					error: function (xhr, status, errorThrown) {
						if (xhr.statusText == "Unauthorized") {
							location.replace("/");
						} else {
							$(".modal-content").LoadingOverlay("hide", true);
							$("#addAnggotaKeluarga").modal("toggle");
							Swal.fire({
								type: "error",
								title: "<b>Gagal!</b>",
								html: xhr.responseJSON.message,
								confirmButtonText: "OK",
							}).then((result) => {
								location.reload();
							});
						}
					},
				});
			}
		}
		$(this).addClass("was-validated");
	});

	$(document).on("click", "#disabled_kecamatan", function () {
		var id = $(this).attr("data-id");

		Swal.fire({
			title: "Yakin ingin mengnonaktifkan Kecamatan ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, Non Aktifkan!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: async () => {
				return fetch(`/kecamatan/${id}/disabled`, {
					method: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(result) => {
						location.reload();
					}
				);
			}
		});
	});

	$(document).on("click", "#enabled_kecamatan", function () {
		var id = $(this).attr("data-id");

		Swal.fire({
			title: "Yakin ingin mengaktifkan Kecamatan ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, Aktifkan!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: async () => {
				return fetch(`/kecamatan/${id}/enabled`, {
					method: "post",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(result) => {
						location.reload();
					}
				);
			}
		});
	});
});
